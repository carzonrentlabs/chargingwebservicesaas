﻿using System;
using System.Collections.Generic;
using ChargingWebService;
using System.Data;
using Newtonsoft.Json;
using RestSharp;

public partial class EInvoicing : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //string str = RandomString(8, true);
        //Task<string> str = PostData();
        //Task<string> str = PostJsonAsync();
        //Response.Write(str);
        //bool status = PostDataTEst();

        //TEsting();
        //EBilled str = doPost();

        EbillingPost(false);

    }

    public void TEsting()
    {
        string str = "{\"Status\":\"0\",\"Data\":\"null\",\"ErrorDetails\":\"[{\"ErrorCode\":\"ASP-3011\",\"ErrorMessage\":\"docnum or inum is mandatory.\"}]\",\"InfoDtls\":\"null\"}";

        str = str.Replace("\"[", "[");
        str = str.Replace("]\"", "]");
        //EBilling responsestring = new EBilling();

        EBilling responsestring = JsonConvert.DeserializeObject<EBilling>(str);
    }

    //var data = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(str);

    public void doPost(string data, string InvoiceNo) //(string fullUrl, string requestJSON, Dictionary<string, string> headers = null)
    {
        //EBilling bill = new EBilling();
        //bill = EbillingPost(false);
        //string data = JsonConvert.SerializeObject(bill);
        //var client = new RestClient("http://13.235.171.254/GO4GST_EINV/rest/eInvWebServiceGovt/eInvERP/VELOCEL?type=generate");
        var client = new RestClient("http://13.235.171.254/GO4GST_EINV/rest/eInvWebService/eInvERP/VELOCEL?type=generate");

        var request = new RestRequest(Method.POST);
        //request.AddHeader("postman-token", "a5e02589-6660-a0d7-1897-af6ccd65aa66");
        request.AddHeader("cache-control", "no-cache");
        request.AddHeader("authorization", "Basic R080R1NUOkdPNEdTVCMxMjM0");
        request.AddHeader("content-type", "application/json");
        request.AddParameter("application/json", data, ParameterType.RequestBody);
        IRestResponse response = client.Execute(request);
        string str = response.Content.ToString();

        EBilled responsestring = new EBilled();

        str = str.Replace("\"[", "[");
        str = str.Replace("]\"", "]");

        responsestring = JsonConvert.DeserializeObject<EBilled>(str);

        if (responsestring.STATUS == "SUCCESS")
        {
            SaveEInvoice(InvoiceNo, responsestring.RESPONSE.SignedQRCode, responsestring.RESPONSE.AckDt, responsestring.RESPONSE.AckNo, responsestring.RESPONSE.Irn);
        }
        //return responsestring;
    }



    public void EbillingPost(bool BulkBooking)
    {
        DataSet ds = new DataSet();
        DataSet dslines = new DataSet();

        EBilling bill = new EBilling();
        List<InvoiceDetails> inv = new List<InvoiceDetails>();
        InvoiceDetails invnew = new InvoiceDetails();
        transaction_details tran = new transaction_details();
        document_details documentinfo = new document_details();
        //export_details exportinfo = new export_details();
        //extra_Information extrainfo = new extra_Information();
        billing_Information billinginfo = new billing_Information();
        shipping_Information shippinginfo = new shipping_Information();
        delivery_Information deliveryinfo = new delivery_Information();
        //payee_Information payeeinfo = new payee_Information();
        //ewaybill_information ewaybillinfo = new ewaybill_information();
        document_Total document_Total = new document_Total();
        List<items> items = new List<items>();
        items item = new items();
        //batch bth = new batch();
        attribDtls attribdtls = new attribDtls();

        if (BulkBooking)
        {
            ds = GetEInvoiceHeader();
        }
        else
        {
            ds = GetEInvoiceDetails();
        }

        if (ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                //**transaction_details start
                tran.transactionMode = "REG"; // to be discussed with Sanjay Arora
                if (Convert.ToBoolean(ds.Tables[0].Rows[i]["SEZClientYN"]))
                {
                    if (Convert.ToDouble(ds.Tables[0].Rows[i]["TotalTaxPercent"]) > 0)
                    {
                        tran.invoice_type_code = "SEZWP";
                    }
                    else
                    {
                        tran.invoice_type_code = "SEZWOP";
                    }
                }
                else
                {
                    tran.invoice_type_code = "B2B";
                }
                tran.reversecharge = "N"; //as discussed with Anurag N is fixed
                tran.ecom_GSTIN = ""; // ds.Tables[0].Rows[i]["GSTIN"].ToString(); made blank to test //change later
                //tran.IgstOnIntra = "N";  //optional so commented
                //**transaction_details end

                //**document_details start
                documentinfo.invoice_subtype_code = "INV";
                documentinfo.invoiceNum = ds.Tables[0].Rows[i]["InvoiceNo"].ToString();
                documentinfo.invoiceDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["AccountingDate"]).ToString("dd-MM-yyyy");
                //documentinfo.transaction_id = "";  //optional so removed
                //documentinfo.plant = "";  //optional so removed
                //documentinfo.custom = "";  //optional so removed
                //**document_details end

                //**export_details start //removed as it's optional
                //exportinfo.shipping_bill_no = "";
                //exportinfo.shipping_bill_date = "";
                //exportinfo.port_code = "";
                //exportinfo.invoice_currency_code = "";
                //exportinfo.cnt_code = "";
                //exportinfo.refClm = "";
                //exportinfo.ExpDuty = "";
                //**export_details end


                //**extra_Information start
                /*
                extrainfo.invoice_Period_Start_Date = ds.Tables[0].Rows[i]["DateOut"].ToString();
                extrainfo.invoice_Period_End_Date = ds.Tables[0].Rows[i]["DateIn"].ToString();
                extrainfo.preceeding_Invoice_Number = ds.Tables[0].Rows[i]["InvoiceNo"].ToString();
                extrainfo.preceeding_Invoice_Date = ds.Tables[0].Rows[i]["AccountingDate"].ToString();
                extrainfo.invoice_Document_Reference = "";
                extrainfo.receipt_Advice_ReferenceNo = "";
                extrainfo.receipt_Advice_ReferenceDt = "";
                extrainfo.tender_or_Lot_Reference = "";
                extrainfo.contract_Reference = "";
                extrainfo.external_Reference = "";
                extrainfo.project_Reference = "";
                extrainfo.refNum = "";
                extrainfo.refDate = "";
                extrainfo.remarks = "";
                extrainfo.Url = "";
                extrainfo.Docs = "";
                extrainfo.Info = "";
                */
                //**extra_Information end


                //**billing_Information start
                billinginfo.billing_Name = ds.Tables[0].Rows[i]["ClientCoName"].ToString();
                billinginfo.billing_trade_name = "";
                billinginfo.billing_GSTIN = ds.Tables[0].Rows[i]["GSTIN"].ToString();
                billinginfo.billing_POS = ds.Tables[0].Rows[i]["StateCode"].ToString();
                billinginfo.billing_City = ds.Tables[0].Rows[i]["CityName"].ToString();
                billinginfo.billing_State = ds.Tables[0].Rows[i]["StateCode"].ToString();
                billinginfo.billing_Address1 = ds.Tables[0].Rows[i]["RegisteredGSTOfficeAddress"].ToString();
                billinginfo.billing_Address2 = "";
                billinginfo.billing_Pincode = ds.Tables[0].Rows[i]["PINCode"].ToString();
                //billinginfo.billing_Phone = "";
                //billinginfo.billing_Email = "";
                //**billing_Information end

                //**shipping_Information start
                shippinginfo.shippingTo_Name = ds.Tables[0].Rows[i]["ClientCoName"].ToString();
                shippinginfo.shippingTo_trade_name = "";
                shippinginfo.shippingTo_GSTIN = "";
                shippinginfo.shippingTo_Place = ds.Tables[0].Rows[i]["CityName"].ToString();
                shippinginfo.shippingTo_State = ds.Tables[0].Rows[i]["StateCode"].ToString();
                shippinginfo.shippingTo_Address1 = ds.Tables[0].Rows[i]["RegisteredGSTOfficeAddress"].ToString();
                shippinginfo.shippingTo_Address2 = "";
                shippinginfo.shippingTo_Pincode = ds.Tables[0].Rows[i]["PINCode"].ToString();
                //**shipping_Information end

                //**delivery_Information start
                deliveryinfo.company_Name = ds.Tables[0].Rows[i]["ClientCoName"].ToString();
                deliveryinfo.city = ds.Tables[0].Rows[i]["CityName"].ToString();
                deliveryinfo.state = ds.Tables[0].Rows[i]["StateCode"].ToString();
                deliveryinfo.address1 = ds.Tables[0].Rows[i]["RegisteredGSTOfficeAddress"].ToString();
                deliveryinfo.address2 = "";
                deliveryinfo.pincode = ds.Tables[0].Rows[i]["PINCode"].ToString();
                //**delivery_Information end

                //**payee_Information start
                /*
                payeeinfo.payee_Name = "";
                payeeinfo.payer_Financial_Account = "";
                payeeinfo.modeofPayment = "";
                payeeinfo.financial_Institution_Branch = "";
                payeeinfo.payment_Terms = "";
                payeeinfo.payment_Instruction = "";
                payeeinfo.credit_Transfer = "";
                payeeinfo.direct_Debit = "";
                payeeinfo.creditDays = "";
                payeeinfo.paid_amount = "";
                payeeinfo.amount_due_for_payment = "";
                */
                //**payee_Information end

                /*
                //**ewaybill_information start
                ewaybillinfo.ewb_transporter_id = ds.Tables[0].Rows[i]["GSTIN"].ToString();
                ewaybillinfo.ewb_transMode = "1";
                ewaybillinfo.ewb_transDistance = ds.Tables[0].Rows[i]["DistanceKM"].ToString();
                ewaybillinfo.ewb_transporterName = "";
                ewaybillinfo.ewb_transDocNo = "test";//ds.Tables[0].Rows[i][""].ToString();
                ewaybillinfo.ewb_transDocDt = ds.Tables[0].Rows[i]["DateOut"].ToString();
                ewaybillinfo.ewb_vehicleNo = ds.Tables[0].Rows[i]["CarNo"].ToString();
                ewaybillinfo.ewb_subSupplyType = "";
                ewaybillinfo.ewb_vehicleType = "R";
                //**ewaybill_information end
                */

                //**document_Total start
                document_Total.roundoff = "0"; //ds.Tables[0].Rows[i]["TotalCost"].ToString(); //if round off > 0 then total round off value
                document_Total.total_assVal = ds.Tables[0].Rows[i]["BeforeTax"].ToString(); //total amount before tax
                document_Total.total_Invoice_Value = ds.Tables[0].Rows[i]["TotalCost"].ToString(); //total amount including tax
                document_Total.igstvalue = ds.Tables[0].Rows[i]["IGSTTaxAmt"].ToString(); //total igst amount
                document_Total.cgstvalue = ds.Tables[0].Rows[i]["CGSTTaxAmt"].ToString(); //total cgst amount 
                document_Total.sgstvalue = ds.Tables[0].Rows[i]["SGSTTaxAmt"].ToString(); //total igst amount

                document_Total.cessvalue = "0"; // ds.Tables[0].Rows[i][""].ToString();
                document_Total.stateCessValue = "0";  //ds.Tables[0].Rows[i][""].ToString();
                document_Total.val_for_cur = "0"; // ds.Tables[0].Rows[i][""].ToString();
                document_Total.Discount = "0"; // ds.Tables[0].Rows[i][""].ToString();
                document_Total.OthChrg = "0"; // ds.Tables[0].Rows[i][""].ToString();
                //**document_Total end



                //**batch start //changes in case booking is bulk batch booking
                /*
                bth.batchName = ""; // ds.Tables[0].Rows[i][""].ToString();
                bth.batchExpiry_Date = ""; // ds.Tables[0].Rows[i][""].ToString();
                bth.warrantyDate = ""; //ds.Tables[0].Rows[i][""].ToString();
                */
                //**batch end

                //**attribDtls start
                attribdtls.attrib_name = ""; // ds.Tables[0].Rows[i][""].ToString();
                attribdtls.attrib_val = ""; // ds.Tables[0].Rows[i][""].ToString();
                //**attribDtls end

                //**items start
                if (BulkBooking)
                {
                    dslines = GetEInvoiceLines(Convert.ToInt32(ds.Tables[0].Rows[i]["InvoiceId"]));

                    for (int k = 0; k <= dslines.Tables[0].Rows.Count - 1; k++)
                    {
                        item.slno = k + 1;
                        item.item_Description = "";
                        item.service = "Y";
                        item.hsn_code = Convert.ToString(dslines.Tables[0].Rows[k]["hsn_code"]);
                        //item.bth = bth; //optional so commented
                        item.barcode = "";
                        item.quantity = 1;  //to be checked if to be posed
                        //item.freeQty = 0; // optional
                        item.uqc = Convert.ToString(dslines.Tables[0].Rows[k]["uqc"]); //to be confired from Sanjay
                        item.rate = Convert.ToDouble(dslines.Tables[0].Rows[k]["BeforeTax"]); //before tax amount
                        item.grossAmount = Convert.ToDouble(dslines.Tables[0].Rows[k]["BeforeTax"]);
                        item.discountAmount = Convert.ToDouble(dslines.Tables[0].Rows[k]["DiscountAmt"]);
                        item.preTaxAmount = Convert.ToDouble(dslines.Tables[0].Rows[k]["TaxAmt"]);
                        item.assesseebleValue = Convert.ToDouble(dslines.Tables[0].Rows[k]["BeforeTax"]);
                        item.igst_rt = Convert.ToDouble(dslines.Tables[0].Rows[k]["IGSTTaxPercent"]);
                        item.cgst_rt = Convert.ToDouble(dslines.Tables[0].Rows[k]["CGSTTaxPercent"]);
                        item.sgst_rt = Convert.ToDouble(dslines.Tables[0].Rows[k]["SGSTTaxPercent"]);
                        item.cess_rt = 0;
                        item.iamt = Convert.ToDouble(dslines.Tables[0].Rows[k]["IGSTTaxAmt"]);
                        item.camt = Convert.ToDouble(dslines.Tables[0].Rows[k]["CGSTTaxAmt"]);
                        item.samt = Convert.ToDouble(dslines.Tables[0].Rows[k]["SGSTTaxAmt"]);
                        item.csamt = 0;
                        //item.cessnonadval = 0; //optional
                        //item.state_cess = 0; // optional
                        //item.stateCessAmt = 0; // optional
                        //item.stateCesNonAdvlAmt = 0; // optional

                        item.otherCharges = 0;
                        item.itemTotal = Convert.ToDouble(ds.Tables[0].Rows[i]["TotalCost"]);

                        item.ordLineRef = 0;
                        item.origin_Country = "IN";
                        item.prdSlNo = "";
                        item.attribDtls = attribdtls;
                        items.Add(item);

                    }

                }
                else
                {
                    item.slno = 1;
                    item.item_Description = "";
                    item.service = "Y";
                    item.hsn_code = Convert.ToString(ds.Tables[0].Rows[i]["hsn_code"]);
                    //item.bth = bth; //optional so commented
                    item.barcode = "";
                    item.quantity = 1;    //to be checked if to be posed
                    //item.freeQty = 0; // optional
                    item.uqc = Convert.ToString(ds.Tables[0].Rows[i]["uqc"]);
                    item.rate = Convert.ToDouble(ds.Tables[0].Rows[i]["BeforeTax"]); //before tax amout
                    item.grossAmount = Convert.ToDouble(ds.Tables[0].Rows[i]["BeforeTax"]);
                    item.discountAmount = Convert.ToDouble(ds.Tables[0].Rows[i]["DiscountAmt"]);
                    item.preTaxAmount = Convert.ToDouble(ds.Tables[0].Rows[i]["TaxAmt"]);
                    item.assesseebleValue = Convert.ToDouble(ds.Tables[0].Rows[i]["BeforeTax"]);
                    item.igst_rt = Convert.ToDouble(ds.Tables[0].Rows[i]["IGSTTaxPercent"]);
                    item.cgst_rt = Convert.ToDouble(ds.Tables[0].Rows[i]["CGSTTaxPercent"]);
                    item.sgst_rt = Convert.ToDouble(ds.Tables[0].Rows[i]["SGSTTaxPercent"]);
                    item.cess_rt = 0; // Convert.ToDouble(ds.Tables[0].Rows[i][""]);
                    item.iamt = Convert.ToDouble(ds.Tables[0].Rows[i]["IGSTTaxAmt"]);
                    item.camt = Convert.ToDouble(ds.Tables[0].Rows[i]["CGSTTaxAmt"]);
                    item.samt = Convert.ToDouble(ds.Tables[0].Rows[i]["SGSTTaxAmt"]);

                    item.csamt = 0; //  

                    //item.cessnonadval = 0; // optional
                    //item.state_cess = 0; // optional
                    //item.stateCessAmt = 0; // optional
                    //item.stateCesNonAdvlAmt = 0; // optional
                    item.otherCharges = 0; // 
                    item.itemTotal = Convert.ToDouble(ds.Tables[0].Rows[i]["TotalCost"]);

                    item.ordLineRef = 0; // 
                    item.origin_Country = "IN";
                    item.prdSlNo = "";
                    item.attribDtls = attribdtls;
                    items.Add(item);
                }
                //**items end

                //**InvoiceDetails start
                invnew.transaction_details = tran;
                invnew.document_details = documentinfo;
                //Inv.export_info = exportinfo;
                //Inv.extra_Info = extrainfo;
                invnew.billing_Information = billinginfo;
                invnew.shipping_Information = shippinginfo;
                invnew.delivery_Information = deliveryinfo;
                //Inv.payee_info = payeeinfo;
                //Inv.ewaybill_info = ewaybillinfo;
                invnew.document_Total = document_Total;
                invnew.items = items;

                inv.Add(invnew);
                //Inv.item = item;
                //**InvoiceDetails end

                bill.supplier_GSTIN = "29AAFPB7029M000"; //ds.Tables[0].Rows[i]["CarzonrentGSTIN"].ToString(); made hardcode to test //change later
                bill.supplier_Legal_Name = ds.Tables[0].Rows[i]["CarzonrentLegalName"].ToString();
                bill.supplier_trading_name = ds.Tables[0].Rows[i]["CarzonrentLegalName"].ToString();
                bill.supplier_City = ds.Tables[0].Rows[i]["CityName"].ToString();
                bill.supplier_Address1 = ds.Tables[0].Rows[i]["CarzonrentGSTregisteredAddress"].ToString();
                bill.supplier_Address2 = ds.Tables[0].Rows[i]["CarzonrentGSTregisteredAddress"].ToString();
                bill.supplier_State = ds.Tables[0].Rows[i]["StateCode"].ToString();
                bill.supplier_Pincode = ds.Tables[0].Rows[i]["PINCode"].ToString();
                //bill.supplier_Phone = ds.Tables[0].Rows[i]["UnitPhone1"].ToString();
                //bill.supplier_Email = ds.Tables[0].Rows[i]["UnitEmailID"].ToString();
                bill.inv = inv;


                //send EInvoice


                string data = JsonConvert.SerializeObject(bill);

                doPost(data, documentinfo.invoiceNum);
                //
            }
        }

        //return bill;
    }

    public DataSet GetEInvoiceDetails()
    {
        DB db = new DB();
        return db.GetEInvoiceDetails();
    }

    public DataSet GetEInvoiceHeader()
    {
        DB db = new DB();
        return db.GetEInvoiceHeader();
    }

    public DataSet GetEInvoiceLines(int InvoiceID)
    {
        DB db = new DB();
        return db.GetEInvoiceLines(InvoiceID);
    }

    public void SaveEInvoice(string InvoiceNo, string SignedQRCode, string AckDt, string AckNo, string Irn)
    {
        DB db = new DB();
        db.SaveEInvoice(InvoiceNo, SignedQRCode, AckDt, AckNo, Irn);
    }
}
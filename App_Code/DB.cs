﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DB
/// </summary>
namespace ChargingWebService
{
    public class DB
    {
        public DB()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet GetValidateForClosing(DateTime DateIn, DateTime CurrentDate)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@DateIn", DateIn);
            param[1] = new SqlParameter("@CurrentDate", CurrentDate);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_ValidateClosingBasedOnAccountingDate", param);
        }

        public DataSet CloseBooking_Confirm(ClosingVariables CV, int BookingID, int ClosedBy)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[81];
                int index = -1;

                param[index += 1] = new SqlParameter("@ParkTollChages", CV.ParkTollChages);
                param[index += 1] = new SqlParameter("@InterstateTax", CV.InterstateTax);
                param[index += 1] = new SqlParameter("@PkgID", CV.PkgID);
                param[index += 1] = new SqlParameter("@PkgRate", CV.PkgRate);
                param[index += 1] = new SqlParameter("@ExtraHr", CV.ExtraHr);
                param[index += 1] = new SqlParameter("@ExtraHrRate", CV.ExtraHrRate);
                param[index += 1] = new SqlParameter("@ExtraKM", CV.ExtraKM);
                param[index += 1] = new SqlParameter("@ExtraKMRate", CV.ExtraKMRate);
                param[index += 1] = new SqlParameter("@NightStayAmt", CV.NightStayAmt);
                param[index += 1] = new SqlParameter("@OutStnAmt", CV.OutStnAmt);
                param[index += 1] = new SqlParameter("@DiscPC", CV.DiscountPC);
                param[index += 1] = new SqlParameter("@TotalCost", CV.TotalCost);
                param[index += 1] = new SqlParameter("@CalculatedAmount", CV.CalculatedAmount);
                param[index += 1] = new SqlParameter("@PaidFullYN", CV.PaidFullYN);
                param[index += 1] = new SqlParameter("@PaidFullDate", null);
                param[index += 1] = new SqlParameter("@PaymentMode", CV.PaymentMode);
                param[index += 1] = new SqlParameter("@TotalHrsUsed", CV.TotalHrsUsed);
                param[index += 1] = new SqlParameter("@NoNight", CV.NoNight);
                param[index += 1] = new SqlParameter("@FuelSurcharge", CV.FuelSurcharge);
                param[index += 1] = new SqlParameter("@ServiceTaxPercent", CV.ServiceTaxPercent);
                param[index += 1] = new SqlParameter("@EduCessPercent", CV.EduCessPercent);
                param[index += 1] = new SqlParameter("@DSTPercent", CV.DSTPercent);
                param[index += 1] = new SqlParameter("@ApprovalAmt", CV.ApprovalAmount);
                param[index += 1] = new SqlParameter("@ApprovalNo", CV.ApprovalNo);
                param[index += 1] = new SqlParameter("@CCTypeID", CV.CCType);
                param[index += 1] = new SqlParameter("@BookingID", BookingID);
                param[index += 1] = new SqlParameter("@transactionid", CV.trackid);
                param[index += 1] = new SqlParameter("@BatchNo1", "");
                param[index += 1] = new SqlParameter("@BatchNo2", "");
                param[index += 1] = new SqlParameter("@kmout", CV.kmout);
                param[index += 1] = new SqlParameter("@DateClose", CV.DateClose);
                param[index += 1] = new SqlParameter("@TimeClose", CV.TimeClose);
                param[index += 1] = new SqlParameter("@KMClose", CV.KMClose);
                param[index += 1] = new SqlParameter("@pointOpeniningKm", CV.pointOpeniningKm);
                param[index += 1] = new SqlParameter("@DateIn", CV.DateIn);
                param[index += 1] = new SqlParameter("@TimeIn", CV.TimeIn);
                param[index += 1] = new SqlParameter("@KMIn", CV.KMIn);
                param[index += 1] = new SqlParameter("@CloseDelayReason", 0);
                param[index += 1] = new SqlParameter("@ClosedBy", ClosedBy);
                param[index += 1] = new SqlParameter("@PaymentStatus", 0);
                param[index += 1] = new SqlParameter("@Service", CV.Service);
                param[index += 1] = new SqlParameter("@FXDGarageRate", CV.FXDGarageRate);
                param[index += 1] = new SqlParameter("@DropOffCityID", CV.CityID);
                param[index += 1] = new SqlParameter("@ServiceTaxAmt", CV.ServiceTaxAmt);
                param[index += 1] = new SqlParameter("@EduTaxAmt", CV.EduTaxAmt);
                param[index += 1] = new SqlParameter("@VatAmt", CV.VatAmt);
                param[index += 1] = new SqlParameter("@PreAuthChk", CV.PreAuthNotRequire); //to check
                param[index += 1] = new SqlParameter("@CardNo", CV.CCNo);
                param[index += 1] = new SqlParameter("@ExpiryDate", CV.ExpYYMM);
                param[index += 1] = new SqlParameter("@GuestOpDate", CV.GuestOpDate);
                param[index += 1] = new SqlParameter("@GuestClDate", CV.GuestClDate);
                param[index += 1] = new SqlParameter("@GuestOpTime", CV.GuestOpTime);
                param[index += 1] = new SqlParameter("@GuestClTime", CV.GuestClTime);
                param[index += 1] = new SqlParameter("@ClientID", CV.ClientCoID);
                param[index += 1] = new SqlParameter("@HduCessPercent", CV.HduCessPercent);
                param[index += 1] = new SqlParameter("@ChargingNRequired", false);
                param[index += 1] = new SqlParameter("@DSStatus", CV.DSStatus); //to check
                param[index += 1] = new SqlParameter("@BookingStatus", CV.BookingStatus); //to check
                param[index += 1] = new SqlParameter("@CustomPkgYN", CV.CustomPkgYN); //to check
                param[index += 1] = new SqlParameter("@BillingBasis", CV.BillingBasis);  //to check
                param[index += 1] = new SqlParameter("@Others", CV.Others);
                param[index += 1] = new SqlParameter("@SwachhBharatTaxPercent", CV.SwachhBharatTaxPercent);
                param[index += 1] = new SqlParameter("@SwachhBharatTaxAmt", CV.SwachhBharatTaxAmt);
                param[index += 1] = new SqlParameter("@KrishiKalyanTaxPercent", CV.KrishiKalyanTaxPercent);
                param[index += 1] = new SqlParameter("@KrishiKalyanTaxAmt", CV.KrishiKalyanTaxAmt);
                param[index += 1] = new SqlParameter("@VoucherAmt", CV.VoucherAmt);
                param[index += 1] = new SqlParameter("@MerchantID", CV.MerchantID);

                param[index += 1] = new SqlParameter("@CGSTTaxPercent", CV.CGSTTaxPercent);
                param[index += 1] = new SqlParameter("@SGSTTaxPercent", CV.SGSTTaxPercent);
                param[index += 1] = new SqlParameter("@IGSTTaxPercent", CV.IGSTTaxPercent);
                param[index += 1] = new SqlParameter("@CGSTTaxAmt", CV.CGSTTaxAmt);
                param[index += 1] = new SqlParameter("@SGSTTaxAmt", CV.SGSTTaxAmt);
                param[index += 1] = new SqlParameter("@IGSTTaxAmt", CV.IGSTTaxAmt);
                param[index += 1] = new SqlParameter("@ClientGSTId", CV.ClientGSTId);
                param[index += 1] = new SqlParameter("@Basic", CV.Basic);
                param[index += 1] = new SqlParameter("@GSTSurchargeAmount", CV.GSTSurchargeAmount);

                param[index += 1] = new SqlParameter("@Remarks", CV.Remarks);

                param[index += 1] = new SqlParameter("@DiscountAmt", CV.DiscountAmt);

                param[index += 1] = new SqlParameter("@accountingdate", CV.Accountingdate);

                param[index += 1] = new SqlParameter("@SanatizationCharges", CV.SanatizationCharges);

                param[index += 1] = new SqlParameter("@beforeDiscountAmt", CV.BasicBeforeDiscount);

                //ds = SqlHelper.ExecuteDataset("ProcDS_Invoice_CorDriveCloseConfirm", param);
                //ds = SqlHelper.ExecuteDataset("ProcDS_Invoice_CorDriveCloseConfirm_GST", param);
                //ds = SqlHelper.ExecuteDataset("ProcDS_Invoice_CorDriveCloseConfirm_GST_New", param);
                //ds = SqlHelper.ExecuteDataset("ProcDS_Invoice_Confirm", param);
                ds = SqlHelper.ExecuteDataset("ProcDS_Invoice_ConfirmClient", param);
            }
            catch (Exception ex)
            {
                ErrorLog.LogErrorToLogFile(ex, "CloseBooking_Confirm" + ":" + BookingID.ToString());
            }

            return ds;
        }

        public string GetPaymateSysRegCodeForIndiv(int ClientCoIndivID)
        {
            try
            {
                DataTable dataTableSysRegCode = new DataTable();
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ClientCoIndivID", ClientCoIndivID);
                dataTableSysRegCode = SqlHelper.ExecuteDatatable("Proc_GetPaymateSysRegCodeForIndiv", param);
                if (dataTableSysRegCode != null && dataTableSysRegCode.Rows.Count > 0)
                {
                    DataRow row = dataTableSysRegCode.Rows[0];
                    return Convert.ToString(row["PaymateSysRegCode"]);
                }
                return null;
            }
            catch (Exception Ex)
            {
                ErrorLog.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
        }

        public DataSet CloseBooking_WithOutParkingToll(ClosingVariables CV, int BookingID, int ClosedBy)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[34];
                int index = -1;

                param[index += 1] = new SqlParameter("@BookingID", BookingID);
                param[index += 1] = new SqlParameter("@ParkTollChages", CV.ParkTollChages);
                param[index += 1] = new SqlParameter("@InterstateTax", CV.InterstateTax);
                param[index += 1] = new SqlParameter("@Others", CV.Others);
                param[index += 1] = new SqlParameter("@ClosedBy", ClosedBy);
                param[index += 1] = new SqlParameter("@ClientID", CV.ClientCoID);
                param[index += 1] = new SqlParameter("@DiscPC", CV.DiscountPC);

                param[index += 1] = new SqlParameter("@DiscountAmt", CV.DiscountAmt);
                param[index += 1] = new SqlParameter("@Basic", CV.Basic);
                param[index += 1] = new SqlParameter("@GSTSurchargeAmount", CV.GSTSurchargeAmount);
                param[index += 1] = new SqlParameter("@TotalCost", CV.TotalCost);
                param[index += 1] = new SqlParameter("@ServiceTaxPercent", CV.ServiceTaxPercent);
                param[index += 1] = new SqlParameter("@EduCessPercent", CV.EduCessPercent);
                param[index += 1] = new SqlParameter("@HduCessPercent", CV.HduCessPercent);
                param[index += 1] = new SqlParameter("@DSTPercent", CV.DSTPercent);
                param[index += 1] = new SqlParameter("@SwachhBharatTaxPercent", CV.SwachhBharatTaxPercent);
                param[index += 1] = new SqlParameter("@KrishiKalyanTaxPercent", CV.KrishiKalyanTaxPercent);
                param[index += 1] = new SqlParameter("@CGSTTaxPercent", CV.CGSTTaxPercent);
                param[index += 1] = new SqlParameter("@SGSTTaxPercent", CV.SGSTTaxPercent);
                param[index += 1] = new SqlParameter("@IGSTTaxPercent", CV.IGSTTaxPercent);
                param[index += 1] = new SqlParameter("@ClientGSTId", CV.ClientGSTId);

                param[index += 1] = new SqlParameter("@ServiceTaxAmt", CV.ServiceTaxAmt);
                param[index += 1] = new SqlParameter("@EduTaxAmt", CV.EduTaxAmt);
                param[index += 1] = new SqlParameter("@VatAmt", CV.VatAmt);
                param[index += 1] = new SqlParameter("@SwachhBharatTaxAmt", CV.SwachhBharatTaxAmt);
                param[index += 1] = new SqlParameter("@KrishiKalyanTaxAmt", CV.KrishiKalyanTaxAmt);
                param[index += 1] = new SqlParameter("@CGSTTaxAmt", CV.CGSTTaxAmt);
                param[index += 1] = new SqlParameter("@SGSTTaxAmt", CV.SGSTTaxAmt);
                param[index += 1] = new SqlParameter("@IGSTTaxAmt", CV.IGSTTaxAmt);

                param[index += 1] = new SqlParameter("@DSStatus", CV.DSStatus); //to check
                param[index += 1] = new SqlParameter("@BookingStatus", CV.BookingStatus); //to check

                param[index += 1] = new SqlParameter("@Remarks", CV.Remarks);
                param[index += 1] = new SqlParameter("@PaymentMode", CV.PaymentMode);
                param[index += 1] = new SqlParameter("@DateIn", CV.DateIn);

                ds = SqlHelper.ExecuteDataset("ProcDS_Invoice_ConfirmWithOutParkingToll", param);
            }
            catch (Exception ex)
            {
                ErrorLog.LogErrorToLogFile(ex, "CloseBooking_WithOutParkingToll" + ":" + BookingID.ToString());
            }

            return ds;
        }

        public void CloseBooking_UpdateStatus(int BookingID, string BookingStatus, string DSStatus)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                int index = -1;

                param[index += 1] = new SqlParameter("@BookingID", BookingID);
                param[index += 1] = new SqlParameter("@DSStatus", DSStatus); //to check
                param[index += 1] = new SqlParameter("@BookingStatus", BookingStatus); //to check

                SqlHelper.ExecuteNonQuery("Proc_UpdateStatusBookingDS", param);
            }
            catch (Exception ex)
            {
                ErrorLog.LogErrorToLogFile(ex, "CloseBooking_UpdateStatus" + ":" + BookingID.ToString());
            }
        }

        public void SaveCardFailureLogs(int BookingID, string ErrorCode, string TransactionID, string ErrorMessage, double Amount, string Provider)
        {
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@BookingID", BookingID);
            param[1] = new SqlParameter("@ErrorCode", ErrorCode);
            param[2] = new SqlParameter("@TransacionID", TransactionID);
            param[3] = new SqlParameter("@ErrorMesage", ErrorMessage);
            param[4] = new SqlParameter("@Amount", Amount);
            param[5] = new SqlParameter("@Provider", Provider);

            SqlHelper.ExecuteNonQuery("Prc_CardFailureLogs", param);
        }

        public DataSet SaveCardLogs(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@BookingID", BookingID);

            return SqlHelper.ExecuteDataset("prc_CreditCardConfirm", param);
        }

        public void deleteCardLogs(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@BookingID", BookingID);

            SqlHelper.ExecuteNonQuery("prc_deleteCreditCardConfirm", param);
        }

        public void ProcUniqueTrackID_Micro1(int BookingID, double TotalCost, string ChargingType)
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@BookingID", BookingID);
            param[1] = new SqlParameter("@Amount", TotalCost);
            param[2] = new SqlParameter("@Provider", ChargingType);

            SqlHelper.ExecuteNonQuery("ProcUniqueTrackID_Micro1", param);
        }

        public DataSet CheckClientGSTDetails(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@BookingID", BookingID);

            return SqlHelper.ExecuteDataset("Prc_CheckClientGSTDetails", param);
        }

        public bool CheckSavedCardStatus(int BookingID)
        {
            bool checkSaveCardLogLink = false;
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand checkSaveCardLog = InstaCon.CreateCommand();
            checkSaveCardLog.CommandText = "select top 1 1 from CorIntSavedCardLog where BookingID = @BookingID ";

            checkSaveCardLog.Parameters.AddWithValue("@BookingID", BookingID);
            if (InstaCon.State != ConnectionState.Open)
            {
                InstaCon.Open();
            }
            DataSet ds = new DataSet();
            SqlDataAdapter objAdapter = new SqlDataAdapter(checkSaveCardLog);
            objAdapter.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                checkSaveCardLogLink = true;
            }

            if (InstaCon.State != ConnectionState.Closed)
            {
                InstaCon.Close();
            }
            return checkSaveCardLogLink;
        }


        public bool CheckChargingLink(int BookingID)
        {
            bool ChargingLink = false;
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand checkChargingLink = InstaCon.CreateCommand();
            checkChargingLink.CommandText = "select top 1 1 from CorIntCreditCardScheduler where BookingID = @BookingID ";

            checkChargingLink.Parameters.AddWithValue("@BookingID", BookingID);
            if (InstaCon.State != ConnectionState.Open)
            {
                InstaCon.Open();
            }
            DataSet ds = new DataSet();
            SqlDataAdapter objAdapter = new SqlDataAdapter(checkChargingLink);
            objAdapter.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                ChargingLink = true;
            }

            if (InstaCon.State != ConnectionState.Closed)
            {
                InstaCon.Close();
            }
            return ChargingLink;
        }

        public bool CheckCompanyGSTDetails(int CityID)
        {
            bool CompanyRegisterdYN = true;
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@CityID", CityID);
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset("prccheckcompanyGSTN", param);
            if (ds.Tables[0].Rows.Count > 0)
            {
                CompanyRegisterdYN = false;
            }

            return CompanyRegisterdYN;
        }

        public string CheckClosingforCurrentMonth(DateTime DateIn, DateTime CurrentDate)
        {
            string strClosingForCurrentMonth = "";

            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@DateIn", DateIn);
                param[1] = new SqlParameter("@CurrentDate", CurrentDate);
                ds = SqlHelper.ExecuteDataset("prc_ValidateClosingBasedOnAccountingDate", param);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows[0]["AllowClosing"].ToString() == "1")
                    {
                        strClosingForCurrentMonth = "";
                    }
                    else
                    {
                        strClosingForCurrentMonth = ds.Tables[0].Rows[0]["BookingStatus"].ToString();
                    }
                }
                else
                {
                    strClosingForCurrentMonth = "Please Contact IT Team, as No Locking Period Exists";
                }
            }
            catch (Exception ex)
            {
                strClosingForCurrentMonth = "";
            }
            return strClosingForCurrentMonth;
        }

        public string CheckClosingAllowed(int ClientCoID, string PaymentMode)
        {
            string ClosingAllowed = "";

            if (PaymentMode.ToUpper() == "CR") // && ClientCoID != 631 && ClientCoID != 2205 && ClientCoID != 714)
            {
                string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();

                SqlConnection InstaCon = new SqlConnection(connectionString);

                SqlCommand checkClosingAllowed = InstaCon.CreateCommand();
                checkClosingAllowed.CommandText = "SELECT TOP 1 BlockDate FROM CorIntBlockClosing WHERE CAST(BlockDate AS DATE) = CAST(GETDATE() AS DATE) AND Active = 1";

                if (InstaCon.State != ConnectionState.Open)
                {
                    InstaCon.Open();
                }
                DataSet dsAllowClosing = new DataSet();
                SqlDataAdapter objAdapterAllowClosing = new SqlDataAdapter(checkClosingAllowed);
                objAdapterAllowClosing.Fill(dsAllowClosing);

                if (dsAllowClosing.Tables[0].Rows.Count > 0)
                {
                    SqlCommand checkClientBlocking = InstaCon.CreateCommand();
                    checkClientBlocking.CommandText = "select top 1 1 from CorIntClientBulkBatchMaster where cast(EffectiveDate as date)<=cast(getdate() as date) and ClientCoID = @ClientCoID and active = 1";
                    checkClientBlocking.Parameters.AddWithValue("@ClientCoID", ClientCoID);
                    if (InstaCon.State != ConnectionState.Open)
                    {
                        InstaCon.Open();
                    }
                    DataSet dsclientblocking = new DataSet();
                    SqlDataAdapter objAdapterclientclosing = new SqlDataAdapter(checkClientBlocking);
                    objAdapterclientclosing.Fill(dsclientblocking);

                    if (dsclientblocking.Tables[0].Rows.Count > 0)
                    {
                        ClosingAllowed = "Closing is blocked for BTC Client for date : "
                            + Convert.ToDateTime(dsAllowClosing.Tables[0].Rows[0]["BlockDate"]).ToString("dd-MMM-yyyy") + ".";
                    }
                }
                if (InstaCon.State != ConnectionState.Closed)
                {
                    InstaCon.Close();
                }
            }
            return ClosingAllowed;
        }

        public void RechargingUnAuthorized()
        {
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand checkUnAuthorizedDetails = InstaCon.CreateCommand();
            //checkUnAuthorizedDetails.CommandText = "select cb.bookingid, m.etravelid, inv.totalcost from CorIntMVlog as a inner join corintcarbooking as cb on cb.bookingid = a.bookingid inner join CorIntMultipleBooking as m on m.bookingid = cb.bookingid inner join corintinvoice as inv on inv.bookingid = m.bookingid where a.message='Unauthorised Access!' and cb.status = 'O' and not exists (select 1 from CorIntCreditCardPreAuthStatus where trackid= cast(cb.bookingid as varchar)) and not exists (select 1 from corintmvlog as c where c.bookingid= a.bookingid and c.errorcode='400') and cb.bookingid = 8971996";
            checkUnAuthorizedDetails.CommandText = "select cb.bookingid, m.etravelid, inv.totalcost from CorIntMVlog as a inner join corintcarbooking as cb on cb.bookingid = a.bookingid inner join CorIntMultipleBooking as m on m.bookingid = cb.bookingid inner join corintinvoice as inv on inv.bookingid = m.bookingid where a.message='Unauthorised Access!' and cb.status = 'O' and not exists (select 1 from CorIntCreditCardPreAuthStatus where trackid= cast(cb.bookingid as varchar)) and not exists (select 1 from corintmvlog as c where c.bookingid= a.bookingid and c.errorcode in ('400','000', '076')) and not exists (select 1 from corintamexbatchsubmitted as d where d.bookingid = a.bookingid) and cast(a.createdate as date) >='2018-05-26'";

            if (InstaCon.State != ConnectionState.Open)
            {
                InstaCon.Open();
            }
            DataSet dsUnAuthorizedDetails = new DataSet();
            SqlDataAdapter objAdapterUnAuthorized = new SqlDataAdapter(checkUnAuthorizedDetails);
            objAdapterUnAuthorized.Fill(dsUnAuthorizedDetails);

            if (dsUnAuthorizedDetails.Tables[0].Rows.Count > 0)
            {
                ChargingWebService.clsAdmin objAdmin = new ChargingWebService.clsAdmin();

                string chargingstatus = "", mailstatus = "";
                for (int i = 0; i <= dsUnAuthorizedDetails.Tables[0].Rows.Count - 1; i++)
                {
                    chargingstatus = objAdmin.checkChargingStatus(Convert.ToInt32(dsUnAuthorizedDetails.Tables[0].Rows[i]["bookingid"])
                        , Convert.ToDouble(dsUnAuthorizedDetails.Tables[0].Rows[i]["totalcost"])
                        , Convert.ToString(dsUnAuthorizedDetails.Tables[0].Rows[i]["etravelid"]));

                    if (chargingstatus == "Charged")
                    {
                        CloseBooking_UpdateStatus(Convert.ToInt32(dsUnAuthorizedDetails.Tables[0].Rows[i]["bookingid"]), "C", "C");

                        mailstatus = objAdmin.SendInvoiceMail(Convert.ToInt32(dsUnAuthorizedDetails.Tables[0].Rows[i]["bookingid"]), false);
                    }
                }
            }

            if (InstaCon.State != ConnectionState.Closed)
            {
                InstaCon.Close();
            }
        }

        public void BulkCorporateClosing()
        {
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand checkBulkCorporateClosingDetails = InstaCon.CreateCommand();
            checkBulkCorporateClosingDetails.CommandText = "select cb.bookingid,CoCCIM.cctype,CoCCIM.ccno,CoCCIM.ispaymatecorporatemodule,cb.cctype, coccim.clientcoindivid from corintcarbooking as cb  inner join CORIntClientCoIndivMaster CoCCIM with (NOLOCK) ON CoCCIM.ClientCoIndivID = CB.ClientCoIndivID  where cb.bookingid in (9201483,9201488,9205251,9207443,9195041,9195044,9147293,9032784,8617793,8598351,9213473,8483952,9209106,9195054,9200762,9200746,9200756,9200724,9204658,9204664,9204680,8419595,8448929,8469933,9103502,9174574,9213457,8862007,8576058,9145387,9143079,9216774,9211696,9211699,9149186,9200653,9207598,9215675,9216108,9195930,9195940,9195943,9210001,9164334,9147644,9157907) and CoCCIM.ispaymatecorporatemodule = 1";

            if (InstaCon.State != ConnectionState.Open)
            {
                InstaCon.Open();
            }
            DataSet dsBulkCorporateClosingDetails = new DataSet();
            SqlDataAdapter objAdapterBulkCorporateClosing = new SqlDataAdapter(checkBulkCorporateClosingDetails);
            objAdapterBulkCorporateClosing.Fill(dsBulkCorporateClosingDetails);

            if (dsBulkCorporateClosingDetails.Tables[0].Rows.Count > 0)
            {
                ChargingWebService.clsAdmin objAdmin = new ChargingWebService.clsAdmin();
                PreAuthResult PVC = new PreAuthResult();

                string mailstatus = "";
                for (int i = 0; i <= dsBulkCorporateClosingDetails.Tables[0].Rows.Count - 1; i++)
                {

                    PVC = objAdmin.NewRegistrationPreauthProcess(Convert.ToInt32(dsBulkCorporateClosingDetails.Tables[0].Rows[i]["bookingid"]));

                    if (PVC.ChargingStatus == "Success")
                    {
                        CloseBooking_UpdateStatus(Convert.ToInt32(dsBulkCorporateClosingDetails.Tables[0].Rows[i]["bookingid"]), "C", "C");

                        mailstatus = objAdmin.SendInvoiceMail(Convert.ToInt32(dsBulkCorporateClosingDetails.Tables[0].Rows[i]["bookingid"]), false);
                    }
                }
            }

            if (InstaCon.State != ConnectionState.Closed)
            {
                InstaCon.Close();
            }
        }


        //manual closing start
        public EditClosingVariables GetEditClosingDetails(EditClosingVariables EditVariable)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            param[0] = new SqlParameter("@BookingId", EditVariable.BookingID);
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetEditClosingDetails", param);

            if (ds.Tables[0].Rows.Count > 0)
            {
                EditVariable.SanatizationCharges = Convert.ToDouble(ds.Tables[0].Rows[0]["SanatizationCharges"]);
                EditVariable.PendingYn = Convert.ToBoolean(ds.Tables[0].Rows[0]["PendingYn"]);
                EditVariable.BillingBasis = Convert.ToString(ds.Tables[0].Rows[0]["BillingBasis"]);
                EditVariable.ClientCoID = Convert.ToInt32(ds.Tables[0].Rows[0]["ClientCoID"]);
                EditVariable.CustomYN = Convert.ToBoolean(ds.Tables[0].Rows[0]["CustomPkgYN"]);
                EditVariable.OutstationYN = Convert.ToBoolean(ds.Tables[0].Rows[0]["OutstationYN"]);
                EditVariable.Service = Convert.ToString(ds.Tables[0].Rows[0]["Service"]);
                EditVariable.DateOut = Convert.ToDateTime(ds.Tables[0].Rows[0]["DateOut"]);
                EditVariable.TimeOut = CorrectTimeLength(Convert.ToString(ds.Tables[0].Rows[0]["TimeOut"]));
                EditVariable.PayMentMode = Convert.ToString(ds.Tables[0].Rows[0]["PaymentMode"]);
                EditVariable.CityID = Convert.ToInt32(ds.Tables[0].Rows[0]["PickUpCityID"]);
                EditVariable.CarModelID = Convert.ToInt32(ds.Tables[0].Rows[0]["ModelID"]);
                EditVariable.ClientType = "C";
                EditVariable.IndicatedPkgID = Convert.ToInt32(ds.Tables[0].Rows[0]["IndicatedPkgID"]);
                EditVariable.LocalOneWayDropYN = Convert.ToBoolean(ds.Tables[0].Rows[0]["LocalOneWayDropYN"]);
                EditVariable.DropoffCityID = Convert.ToInt32(ds.Tables[0].Rows[0]["DropOffCityID"]);

                EditVariable.CarID = Convert.ToInt32(ds.Tables[0].Rows[0]["CarID"]);
                EditVariable.VendorCarYN = Convert.ToBoolean(ds.Tables[0].Rows[0]["VendorCarYN"]);
                EditVariable.SubsidiaryID = Convert.ToInt32(ds.Tables[0].Rows[0]["SubsidiaryID"]);
                if (EditVariable.BillingBasis == "pp" && EditVariable.Service != "T" && !EditVariable.OutstationYN)
                {
                    EditVariable.PkgKmTrue = EditVariable.GuestKmIn - EditVariable.GuestKmOut;

                    EditVariable.StartDateTime = Convert.ToDateTime(EditVariable.GuestDateOut.ToShortDateString()).AddHours
                        (Convert.ToInt32(EditVariable.GuestTimeOut.Substring(0, 2))).AddMinutes(Convert.ToInt32(EditVariable.GuestTimeOut.Substring(2, 2)));

                    EditVariable.EndDateTime = Convert.ToDateTime(EditVariable.GuestDateIn.ToShortDateString()).AddHours
                        (Convert.ToInt32(EditVariable.GuestTimeIn.Substring(0, 2))).AddMinutes(Convert.ToInt32(EditVariable.GuestTimeIn.Substring(2, 2)));

                    EditVariable.PkgKmTrue = EditVariable.GuestKmIn - EditVariable.GuestKmOut;
                }
                else
                {
                    EditVariable.PkgKmTrue = EditVariable.GarageKmIn - EditVariable.GarageKmOut;

                    EditVariable.StartDateTime = Convert.ToDateTime(EditVariable.DateOut.ToShortDateString()).AddHours
                        (Convert.ToInt32(EditVariable.TimeOut.Substring(0, 2))).AddMinutes(Convert.ToInt32(EditVariable.TimeOut.Substring(2, 2)));

                    EditVariable.EndDateTime = Convert.ToDateTime(EditVariable.DateIn.ToShortDateString()).AddHours
                        (Convert.ToInt32(EditVariable.TimeIn.Substring(0, 2))).AddMinutes(Convert.ToInt32(EditVariable.TimeIn.Substring(2, 2)));

                    EditVariable.PkgKmTrue = EditVariable.GarageKmIn - EditVariable.GarageKmOut;
                }

                TimeSpan tm = EditVariable.EndDateTime.Subtract(EditVariable.StartDateTime);

                EditVariable.TotalHr = (tm.TotalMinutes) / 60;
                EditVariable.TotalMin = tm.TotalMinutes;

                EditVariable.ApplyHigherExtraAmt = Convert.ToBoolean(ds.Tables[0].Rows[0]["ApplyHigherExtraAmt"]);
                EditVariable.DiscPC = Convert.ToDouble(ds.Tables[0].Rows[0]["IndicatedDiscPC"]);
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["DiscountAmt"])))
                {
                    EditVariable.DiscAmt = Convert.ToDouble(ds.Tables[0].Rows[0]["DiscountAmt"]);
                }
                else
                {
                    EditVariable.DiscAmt = 0;
                }
                EditVariable.DiscType = Convert.ToString(ds.Tables[0].Rows[0]["DiscountType"]);

                EditVariable.ApprovalNo = Convert.ToString(ds.Tables[0].Rows[0]["ApprovalNo"]);
                EditVariable.CCType = Convert.ToInt32(ds.Tables[0].Rows[0]["CCType"]);
                EditVariable.Trackid = Convert.ToString(ds.Tables[0].Rows[0]["trackid"]);
                EditVariable.PreAuthNotRequire = Convert.ToBoolean(ds.Tables[0].Rows[0]["PreAuthNotRequire"]);
                EditVariable.CCNo = Convert.ToString(ds.Tables[0].Rows[0]["CCNo"]);
                EditVariable.ExpYYMM = Convert.ToString(ds.Tables[0].Rows[0]["ExpYYMM"]);
                EditVariable.chargingstatus = Convert.ToString(ds.Tables[0].Rows[0]["chargingstatus"]);
                EditVariable.DSStatus = Convert.ToString(ds.Tables[0].Rows[0]["Status"]);

                EditVariable.SendApprovalLinkYN = Convert.ToBoolean(ds.Tables[0].Rows[0]["SendApprovalLinkYN"]);

                EditVariable.ServiceTypeID = Convert.ToInt32(ds.Tables[0].Rows[0]["ServiceTypeID"]);
                EditVariable.FuelSurcharge = Convert.ToDouble(ds.Tables[0].Rows[0]["FuelSurcharge"]);
            }

            return EditVariable;
        }

        public string CorrectTimeLength(string Time)
        {
        label:
            if (Time.Length < 4)
            {
                Time = "0" + Time;
                if (Time.Length < 4)
                {
                    goto label;
                }
            }
            return Time;
        }

        public EditClosingVariables GetTimeInterval(EditClosingVariables EditVariable)
        {
            SqlParameter[] param = new SqlParameter[16];
            param[0] = new SqlParameter("@ClientCoID", EditVariable.ClientCoID);
            param[1] = new SqlParameter("@Service", EditVariable.Service);
            param[2] = new SqlParameter("@BillingBasis", EditVariable.BillingBasis);
            param[3] = new SqlParameter("@OutstationYN", EditVariable.OutstationYN);

            param[4] = new SqlParameter("@CustomYN", EditVariable.CustomYN);
            param[5] = new SqlParameter("@GuestDateOut", EditVariable.GuestDateOut);
            param[6] = new SqlParameter("@GuestDateIn", EditVariable.GuestDateIn);
            param[7] = new SqlParameter("@GuestTimeOut", EditVariable.GuestTimeOut);
            param[8] = new SqlParameter("@GuestTimeIn", EditVariable.GuestTimeIn);

            param[9] = new SqlParameter("@GarageDateOut", EditVariable.DateOut);
            param[10] = new SqlParameter("@GarageDateIn", EditVariable.DateIn);
            param[11] = new SqlParameter("@GarageTimeOut", EditVariable.TimeOut);
            param[12] = new SqlParameter("@GarageTimeIn", EditVariable.TimeIn);

            param[13] = new SqlParameter("@PkgHrTrue", EditVariable.PkgHrTrue);
            param[13].Direction = ParameterDirection.Output;

            param[14] = new SqlParameter("@intNoNights", EditVariable.IntNoNights);
            param[14].Direction = ParameterDirection.Output;

            param[15] = new SqlParameter("@intNoDays", EditVariable.IntNoDays);
            param[15].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prcGetTimeInterval", param);

            EditVariable.TotalHr = (double)param[13].Value;
            EditVariable.IntNoNights = (int)param[14].Value;
            EditVariable.IntNoDays = (int)param[15].Value;

            return EditVariable;
        }

        public EditClosingVariables CheckClosingCategory(EditClosingVariables EditVariable)
        {
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();
            param[0] = new SqlParameter("@BookingID", EditVariable.BookingID);
            param[1] = new SqlParameter("@ClientCoID", EditVariable.ClientCoID);
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_CompanyCarModel", param);
            if (ds.Tables[0].Rows.Count > 0)
            {
                EditVariable.CarCatID = Convert.ToInt32(ds.Tables[0].Rows[0]["CarCatID"]);
                EditVariable.CorTax = Convert.ToDouble(ds.Tables[0].Rows[0]["CorTax"]);
                EditVariable.ClosingStatus = "Success";
            }

            string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand checkCarCatID = InstaCon.CreateCommand();
            checkCarCatID.CommandText = "Select top 1 CarcatID FROM dbo.ConIntCompanyCarModelCategoryMaster WHERE CLientCOid = @ClientCoID AND CarModelID = @CarModel AND (ISNULL(cityid,0) = @CityID OR ISNULL(cityid,0) = 0)";
            checkCarCatID.Parameters.AddWithValue("@ClientCoID", EditVariable.ClientCoID);
            checkCarCatID.Parameters.AddWithValue("@CarModel", EditVariable.CarModelID);
            checkCarCatID.Parameters.AddWithValue("@CityID", EditVariable.CityID);

            if (InstaCon.State != ConnectionState.Open)
            {
                InstaCon.Open();
            }
            DataSet dsCarCatID = new DataSet();
            SqlDataAdapter objAdapterClientCoID = new SqlDataAdapter(checkCarCatID);
            objAdapterClientCoID.Fill(dsCarCatID);

            if (dsCarCatID.Tables[0].Rows.Count > 0)
            {
                EditVariable.CarCatID = Convert.ToInt32(dsCarCatID.Tables[0].Rows[0]["CarcatID"]);
            }
            if (InstaCon.State != ConnectionState.Closed)
            {
                InstaCon.Close();
            }

            return EditVariable;
        }

        public EditClosingVariables GetPackageDetails(EditClosingVariables EditVariable)
        {
            DataSet PackageDetails = new DataSet();
            PackageCalc pkg = new PackageCalc();
            if (EditVariable.Service == "C" && !EditVariable.OutstationYN && !EditVariable.CustomYN) //local
            {
                PackageDetails = pkg.DS_GetPackageS1_ModelWise_Easycabs1(EditVariable);  //ProcDS_GetPackageS1_ModelWise_Easycabs1
                if (PackageDetails.Tables[0].Rows.Count > 0)
                {
                    if ((EditVariable.TotalHr > (Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["PkgHrs"])
                        + Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["ThresholdExtraHr"]))
                        ) && Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["ThresholdExtraHr"]) != 0)
                    {
                        PackageDetails.Clear();

                        PackageDetails = pkg.DS_GetPackageHr_ModelWise_Easycabs1(EditVariable);  //ProcDS_GetPackageHr_ModelWise_Easycabs1
                        if (PackageDetails.Tables[0].Rows.Count <= 0)
                        {
                            PackageDetails = pkg.DS_GetPackageS1_ModelWise_Easycabs1(EditVariable);  //ProcDS_GetPackageS1_ModelWise_Easycabs1
                        }
                    }

                    bool PkgKMYN = false;

                    if (PackageDetails.Tables[0].Rows.Count > 0 && (EditVariable.Unitflag == 1 || EditVariable.ClientCoID == 751 || EditVariable.ClientCoID == 2631
                    || EditVariable.ClientCoID == 1527 || EditVariable.ClientCoID == 3180))
                    {
                        if (EditVariable.ClientCoID == 751 || EditVariable.ClientCoID == 2631)
                        {

                            if ((EditVariable.TotalKm - Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["PkgKMs"])) < 0)
                            {
                                PkgKMYN = true;
                            }
                            else
                            {
                                if ((EditVariable.TotalKm - Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["PkgKMs"])) >= (Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["ThresholdExtraKM"]) - 1))
                                {
                                    PkgKMYN = true;
                                }
                                else
                                {
                                    PkgKMYN = false;
                                }
                            }
                        }
                        else
                        {
                            if ((EditVariable.TotalKm - Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["PkgKMs"])) >= Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["ThresholdExtraKM"]))
                            {
                                PkgKMYN = true;
                            }
                            else
                            {
                                PkgKMYN = false;
                            }
                        }
                    }

                    if (Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["ThresholdExtraKM"]) != 0 && PkgKMYN)
                    {
                        PackageDetails.Clear();
                        if (EditVariable.ClientCoID == 751 || EditVariable.ClientCoID == 2631 || EditVariable.ClientCoID == 1527 || EditVariable.ClientCoID == 3180)
                        {
                            PackageDetails = pkg.DS_GetPackageKms_With_Hrs_ModelWise1(EditVariable); //ProcDS_GetPackageKms_With_Hrs_ModelWise1
                        }
                        else
                        {
                            PackageDetails = pkg.DS_GetPackageKMS1_ModelWise_Easycabs1(EditVariable); //ProcDS_GetPackageKMS1_ModelWise_Easycabs1
                        }
                    }

                    if (PackageDetails.Tables[0].Rows.Count > 0 && EditVariable.ClientCoID != 751 && EditVariable.ClientCoID != 2631
                        && EditVariable.ClientCoID != 1527 && EditVariable.ClientCoID != 3180)
                    {
                        if (Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["ThresholdExtraKM"]) != 0
                            && ((EditVariable.TotalKm - Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["PkgKMs"]))
                            >= Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["ThresholdExtraKM"])))
                        {
                            PackageDetails.Clear();

                            PackageDetails = pkg.DS_GetPackageKM_ModelWise_Easycabs1(EditVariable); //ProcDS_GetPackageKM_ModelWise_Easycabs1
                            if (PackageDetails.Tables[0].Rows.Count <= 0)
                            {
                                PackageDetails = pkg.DS_GetPackageKMS1_ModelWise_Easycabs1(EditVariable); //ProcDS_GetPackageKMS1_ModelWise_Easycabs1
                            }
                        }
                    }
                }
                else
                {
                    EditVariable.ClosingStatus = "No Pkg Exists.";
                }
            }
            else if (EditVariable.Service == "C" && EditVariable.OutstationYN && !EditVariable.CustomYN)  //outstation
            {
                if (EditVariable.DropoffCityID == 0)
                {
                    EditVariable.ToNFro = 0;
                }
                EditVariable.Flag = 2;
                if (EditVariable.ClientCoID == 2205 || EditVariable.ClientCoID == 2257)
                {
                    PackageDetails = pkg.DS_PackageOutStation_Intercity(EditVariable); //ProcDS_PackageOutStation_Intercity
                }
                else
                {
                    PackageDetails = pkg.DS_PackageOutStation_ModelWise1(EditVariable); //ProcDS_PackageOutStation_ModelWise1
                }
            }
            else if (EditVariable.Service == "C" && !EditVariable.OutstationYN && EditVariable.CustomYN) //custom
            {
                PackageDetails = pkg.CustomPkg_EasyCabs(EditVariable); //Prc_CustomPkg_EasyCabs
            }
            else if (EditVariable.Service == "A") //Airport
            {
                EditVariable.Flag = 2;
                PackageDetails = pkg.DS_PackageAirport(EditVariable); //ProcDS_PackageAirport
                if (EditVariable.CarCatID != 25)
                {
                    if (PackageDetails.Tables[0].Rows.Count > 0)
                    {
                        if (((EditVariable.PkgKmTrue - Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["PkgKMs"]))
                            >= Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["ThresholdExtraKM"]))
                            && Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["ThresholdExtraKM"]) != 0)
                        {
                            EditVariable.Flag = 3;
                            PackageDetails = pkg.DS_PackageAirport(EditVariable); //ProcDS_PackageAirport
                        }
                    }
                }
            }
            else if (EditVariable.Service == "T") //City Transfer
            {
                if (EditVariable.DropoffCityID > 0)
                {
                    PackageDetails = pkg.GetOneWayCityTransferPackage(EditVariable);  //GetOneWayCityTransferPackage
                }
                else
                {
                    PackageDetails = pkg.DS_PackageCty_ModelWise1(EditVariable);  //ProcDS_PackageCty_ModelWise1
                }
            }

            if (PackageDetails.Tables[0].Rows.Count > 0)
            {
                EditVariable.ClosingStatus = "Success";
                EditVariable.PkgID = Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["PkgID"]);
                EditVariable.PkgRate = Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["PkgRate"]);
                EditVariable.PkgHrs = Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["PkgHrs"]);
                EditVariable.PkgKMs = Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["PkgKMs"]);
                EditVariable.ExtraHrRate = Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["ExtraHrRate"]);
                EditVariable.ExtraKMRate = Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["ExtraKMRate"]);

                if (PackageDetails.Tables[0].Columns.Contains("OutStationAllowance"))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(PackageDetails.Tables[0].Rows[0]["OutStationAllowance"])))
                    {
                        EditVariable.OutStationAllowance = 0;
                    }
                    else
                    {
                        EditVariable.OutStationAllowance = Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["OutStationAllowance"]);
                    }
                }
                else
                {
                    EditVariable.OutStationAllowance = 0;
                }

                if (PackageDetails.Tables[0].Columns.Contains("NightStayAllowance"))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(PackageDetails.Tables[0].Rows[0]["NightStayAllowance"])))
                    {
                        EditVariable.NightStayAllowance = 0;
                    }
                    else
                    {
                        EditVariable.NightStayAllowance = Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["NightStayAllowance"]);
                    }
                }
                else
                {
                    EditVariable.NightStayAllowance = 0;
                }

                if (string.IsNullOrEmpty(Convert.ToString(PackageDetails.Tables[0].Rows[0]["ThresholdExtraHr"])))
                {
                    EditVariable.ThresholdExtraHr = 0;
                }
                else
                {
                    EditVariable.ThresholdExtraHr = Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["ThresholdExtraHr"]);
                }
                if (string.IsNullOrEmpty(Convert.ToString(PackageDetails.Tables[0].Rows[0]["ThresholdExtraKM"])))
                {
                    EditVariable.ThresholdExtraKM = 0;
                }
                else
                {
                    EditVariable.ThresholdExtraKM = Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["ThresholdExtraKM"]);
                }

                if (PackageDetails.Tables[0].Columns.Contains("WaitingCharges"))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(PackageDetails.Tables[0].Rows[0]["WaitingCharges"])))
                    {
                        EditVariable.WaitingCharges = 0;
                    }
                    else
                    {
                        EditVariable.WaitingCharges = Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["WaitingCharges"]);
                    }
                }
                else
                {
                    EditVariable.WaitingCharges = 0;
                }

                if (PackageDetails.Tables[0].Columns.Contains("NoOfNgts"))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(PackageDetails.Tables[0].Rows[0]["NoOfNgts"])))
                    {
                        EditVariable.NoOfNgts = 0;
                    }
                    else
                    {
                        EditVariable.NoOfNgts = Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["NoOfNgts"]);
                    }
                }
                else
                {
                    EditVariable.NoOfNgts = 0;
                }

                if (PackageDetails.Tables[0].Columns.Contains("NoOfDys"))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(PackageDetails.Tables[0].Rows[0]["NoOfDys"])))
                    {
                        EditVariable.NoOfDys = 0;
                    }
                    else
                    {
                        EditVariable.NoOfDys = Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["NoOfDys"]);
                    }
                }
                else
                {
                    EditVariable.NoOfDys = 0;
                }

                if (PackageDetails.Tables[0].Columns.Contains("ExNgtAmt"))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(PackageDetails.Tables[0].Rows[0]["ExNgtAmt"])))
                    {
                        EditVariable.ExNgtAmt = 0;
                    }
                    else
                    {
                        EditVariable.ExNgtAmt = Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["ExNgtAmt"]);
                    }
                }
                else
                {
                    EditVariable.ExNgtAmt = 0;
                }

                if (PackageDetails.Tables[0].Columns.Contains("ExdayAmt"))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(PackageDetails.Tables[0].Rows[0]["ExdayAmt"])))
                    {
                        EditVariable.ExdayAmt = 0;
                    }
                    else
                    {
                        EditVariable.ExdayAmt = Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["ExdayAmt"]);
                    }
                }
                else
                {
                    EditVariable.ExdayAmt = 0;
                }

                if (string.IsNullOrEmpty(Convert.ToString(PackageDetails.Tables[0].Rows[0]["IsMinuteWiseBilling"])))
                {
                    EditVariable.IsMinuteWiseBilling = false;
                }
                else
                {
                    EditVariable.IsMinuteWiseBilling = Convert.ToBoolean(PackageDetails.Tables[0].Rows[0]["IsMinuteWiseBilling"]);
                }
                if (string.IsNullOrEmpty(Convert.ToString(PackageDetails.Tables[0].Rows[0]["NightChargeMultiplier"])))
                {
                    EditVariable.NightChargeMultiplier = false;
                }
                else
                {
                    EditVariable.NightChargeMultiplier = Convert.ToBoolean(PackageDetails.Tables[0].Rows[0]["NightChargeMultiplier"]);
                }
                //EditVariable.CGSTTaxPercent = Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["CGSTTaxPercent"]);
                //EditVariable.SGSTTaxPercent = Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["SGSTTaxPercent"]);
                //EditVariable.IGSTTaxPercent = Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["IGSTTaxPercent"]);

                //EditVariable.ClientGSTId = Convert.ToInt32(PackageDetails.Tables[0].Rows[0]["ClientGSTId"]);
                //if (string.IsNullOrEmpty(Convert.ToString(PackageDetails.Tables[0].Rows[0]["GSTSurchargeAmount"])))
                //{
                //    EditVariable.GSTSurchargeAmount = 0;
                //}
                //else
                //{
                //    EditVariable.GSTSurchargeAmount = Convert.ToDouble(PackageDetails.Tables[0].Rows[0]["GSTSurchargeAmount"]);
                //}
                //if (string.IsNullOrEmpty(Convert.ToString(PackageDetails.Tables[0].Rows[0]["GSTSurchargeEffectiveDate"])))
                //{
                //    EditVariable.GSTSurchargeEffectiveDate = null;
                //}
                //else
                //{
                //EditVariable.GSTSurchargeEffectiveDate = Convert.ToDateTime(PackageDetails.Tables[0].Rows[0]["GSTSurchargeEffectiveDate"]);
                //}
                if (string.IsNullOrEmpty(Convert.ToString(PackageDetails.Tables[0].Rows[0]["LocalOneWayDropYN"])))
                {
                    EditVariable.LocalOneWayDropYN = false;
                }
                else
                {
                    EditVariable.LocalOneWayDropYN = Convert.ToBoolean(PackageDetails.Tables[0].Rows[0]["LocalOneWayDropYN"]);
                }

                if (EditVariable.PkgHrs >= EditVariable.TotalHr)
                {
                    EditVariable.ThresholdExtraHr = 0;
                }
                else
                {
                    EditVariable.ThresholdExtraHr = EditVariable.TotalHr - EditVariable.PkgHrs;
                }


                string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();

                SqlConnection InstaCon = new SqlConnection(connectionString);

                SqlCommand checkclientmaster = InstaCon.CreateCommand();
                checkclientmaster.CommandText = "SELECT top 1 1 FROM dbo.corintclientcomaster WHERE clientconame like '%price%water%' AND clientcoid = @ClientCoID";
                checkclientmaster.Parameters.AddWithValue("@ClientCoID", EditVariable.ClientCoID);

                if (InstaCon.State != ConnectionState.Open)
                {
                    InstaCon.Open();
                }
                DataSet dscheckclientmaster = new DataSet();
                SqlDataAdapter objAdaptercheckclientmaster = new SqlDataAdapter(checkclientmaster);
                objAdaptercheckclientmaster.Fill(dscheckclientmaster);
                double minscalc = 0;
                if (dscheckclientmaster.Tables[0].Rows.Count > 0)
                {
                    if (EditVariable.ThresholdExtraHr > 0)
                    {
                        string[] ExtraHr = EditVariable.ThresholdExtraHr.ToString().Split('.');
                        if (Convert.ToDouble(ExtraHr[1]) >= 0)
                        {
                            if (Convert.ToDouble(ExtraHr[1]) == 0)
                            {
                                minscalc = 0;
                            }
                            else
                            {
                                if (Convert.ToDouble(ExtraHr[1]) <= 50)
                                {
                                    minscalc = 50;
                                }
                                else
                                {
                                    minscalc = 100;
                                }
                            }
                            EditVariable.ThresholdExtraHr = Convert.ToDouble(ExtraHr[0]) + (minscalc / 100);
                        }
                    }
                    else
                    {
                        EditVariable.ThresholdExtraHr = 0;
                    }
                }
                if (string.IsNullOrEmpty(Convert.ToString(EditVariable.ThresholdExtraHr)))
                {
                    EditVariable.ThresholdExtraHr = 0;
                }

                if (EditVariable.PkgKMs >= EditVariable.PkgKmTrue)
                {
                    EditVariable.ThresholdExtraKM = 0;
                }
                else
                {
                    if (EditVariable.OutstationYN)
                    {
                        EditVariable.PkgKMs = EditVariable.PkgKMs * EditVariable.PkgHrTrue; //to check

                        if ((EditVariable.PkgKmTrue - EditVariable.PkgKMs) < 0)
                        {
                            EditVariable.ThresholdExtraKM = 0;
                        }
                        else
                        {
                            EditVariable.ThresholdExtraKM = EditVariable.PkgKmTrue - EditVariable.PkgKMs;
                        }
                    }
                    else
                    {
                        EditVariable.ThresholdExtraKM = EditVariable.PkgKmTrue - EditVariable.PkgKMs;
                    }
                }

                if (EditVariable.IsMinuteWiseBilling || EditVariable.LocalOneWayDropYN)
                {
                    if (EditVariable.LocalOneWayDropYN && !EditVariable.OutstationYN)
                    {
                        EditVariable.ExtraHrAmt = (EditVariable.TotalMin - EditVariable.PkgHrs) * EditVariable.ExtraHrRate;
                        EditVariable.TotalHr = EditVariable.TotalMin;
                        EditVariable.ThresholdExtraHr = EditVariable.TotalMin - EditVariable.PkgHrs;

                        if (EditVariable.ThresholdExtraHr < 0)
                        {
                            EditVariable.ThresholdExtraHr = 0;
                        }
                    }
                    else
                    {
                        EditVariable.ExtraHrAmt = EditVariable.TotalMin * (EditVariable.ExtraHrRate / 60);
                    }
                }
                else
                {
                    EditVariable.ExtraHrAmt = EditVariable.ThresholdExtraHr * EditVariable.ExtraHrRate;
                }
            }
            else
            {
                EditVariable.ClosingStatus = "No Pkg Exists.";
            }

            EditVariable.ExtraKMAmt = EditVariable.ThresholdExtraKM * EditVariable.ExtraKMRate;

            return EditVariable;
        }

        public EditClosingVariables GetFGRDetails(EditClosingVariables EditVariable)
        {
            if ((EditVariable.ClientCoID == 1288 || EditVariable.ClientCoID == 1861 || EditVariable.ClientCoID == 1862
                || EditVariable.ClientCoID == 1863 || EditVariable.ClientCoID == 1864) && EditVariable.BillingBasis == "pp")
            {
                EditVariable.FGROut = EditVariable.GuestKmOut - EditVariable.GarageKmOut;
                EditVariable.FGRIn = EditVariable.GarageKmIn - EditVariable.GuestKmIn;
                EditVariable.TotalFGR = EditVariable.FGROut + EditVariable.FGRIn;
                if (EditVariable.TotalFGR > 0)
                {
                    EditVariable.FixedGarageRun = EditVariable.ExtraKMRate * EditVariable.TotalFGR;
                }
            }
            else
            {
                SqlParameter[] param = new SqlParameter[7];
                param[0] = new SqlParameter("@CityID", EditVariable.CityID);
                param[1] = new SqlParameter("@Service", EditVariable.Service);
                param[2] = new SqlParameter("@CompanyID", EditVariable.ClientCoID);
                param[3] = new SqlParameter("@CarCatID", EditVariable.CarCatID);

                param[4] = new SqlParameter("@OutStationYN", EditVariable.OutstationYN);
                param[5] = new SqlParameter("@DateOut", EditVariable.DateOut);

                param[6] = new SqlParameter("@Amount", EditVariable.FixedGarageRun);
                param[6].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "ProcDS_FindFixedGarageRunAMT_EffectiveDate", param);

                EditVariable.FixedGarageRun = (double)param[6].Value;
            }

            if ((EditVariable.FixedGarageRun > 0 && EditVariable.BillingBasis == "pp") &&
                (EditVariable.ClientCoID == 2354 || EditVariable.ClientCoID == 2442 || EditVariable.ClientCoID == 2900
                || EditVariable.ClientCoID == 3509 || EditVariable.ClientCoID == 3410 || EditVariable.ClientCoID == 3468))
            {
                if ((EditVariable.PkgKMs - EditVariable.ThresholdExtraKM) > 0)
                {
                    EditVariable.FGRFraction = (20 - (EditVariable.PkgKMs - EditVariable.ThresholdExtraKM)) / 20;
                    if (EditVariable.FGRFraction <= 0)
                    {
                        EditVariable.FixedGarageRun = 0;
                    }
                    else
                    {
                        EditVariable.FixedGarageRun = EditVariable.FixedGarageRun * EditVariable.FGRFraction;
                    }
                }
            }

            if (EditVariable.Service == "A" || (EditVariable.Service == "C" && EditVariable.OutstationYN))
            {
                EditVariable.FixedGarageRun = 0;
            }

            return EditVariable;
        }

        public double GetFuelSurchargeDetails(EditClosingVariables EditVariable)
        {
            double FuelAmount = 0;
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@BookingID", EditVariable.BookingID);
            param[1] = new SqlParameter("@StartKM", EditVariable.GuestKmOut);
            param[2] = new SqlParameter("@EndKM", EditVariable.GuestKmIn);
            param[3] = new SqlParameter("@GarageOpeningKM", EditVariable.GarageKmOut);

            param[4] = new SqlParameter("@GarageClosingKM", EditVariable.GarageKmIn);
            param[5] = new SqlParameter("@VendorCarYN", EditVariable.VendorCarYN);
            param[6] = new SqlParameter("@carID", EditVariable.CarID);
            param[7] = new SqlParameter("@ClientCoID", EditVariable.ClientCoID);

            param[8] = new SqlParameter("@FuelAmount", EditVariable.FuelAmount);
            param[8].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_CalculateFuelsurcharge_Latest", param);

            FuelAmount = (double)param[8].Value;

            return FuelAmount;
        }

        public EditClosingVariables CalculateAmt(EditClosingVariables EditVariable)
        {


            return EditVariable;
        }
        //manual closing end

        public void SaveMVLog(string bookingId, string transactionId, string PaymentCode, string Message, string Provider)
        {
            try
            {
                string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();
                SqlConnection InstaCon1 = new SqlConnection(connectionString);

                InstaCon1.Open();

                SqlCommand insertcommand = InstaCon1.CreateCommand();
                insertcommand.CommandText = @" insert into dbo.corintmvlog(approvalno, bookingid, createdate, errorcode, transactionid, message
                , provider, manualchargingyn) values(@BookingID, @BookingID, getdate(), @PaymentCode, @transactionId, @Message, @Provider, 0)";
                insertcommand.Parameters.AddWithValue("@BookingID", bookingId);
                insertcommand.Parameters.AddWithValue("@PaymentCode", PaymentCode);
                insertcommand.Parameters.AddWithValue("@transactionId", transactionId);
                insertcommand.Parameters.AddWithValue("@Message", Message);
                insertcommand.Parameters.AddWithValue("@Provider", Provider);

                insertcommand.ExecuteNonQuery();
                if (InstaCon1 != null) InstaCon1.Close();
            }
            catch (Exception Ex)
            {
                ErrorLog.LogErrorToLogFile(Ex, "error in SaveMVLog method:");
            }
        }

        public Convenience GetConvenienceYN(EditClosingVariables EditVariable)
        {
            Convenience conv = new Convenience();
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand checkConvenienceFee = InstaCon.CreateCommand();
            checkConvenienceFee.CommandText = "select top 1 CM.CityID, ISNULL(CM.AddtionalKm,0) as AddtionalKm, conv.ID from dbo.CORIntCityMaster as CM with (nolock) left outer join dbo.CorintConvenienceFeesMaster as conv with (nolock) on CM.CityID = conv.CityID WHERE CM.CityID = @CityID";
            checkConvenienceFee.Parameters.AddWithValue("@CityID", EditVariable.CityID);

            if (InstaCon.State != ConnectionState.Open)
            {
                InstaCon.Open();
            }
            DataSet dsConvenienceFee = new DataSet();
            SqlDataAdapter objAdapterConvenienceFee = new SqlDataAdapter(checkConvenienceFee);
            objAdapterConvenienceFee.Fill(dsConvenienceFee);

            if (dsConvenienceFee.Tables[0].Rows.Count > 0)
            {
                if (EditVariable.ClientCoID == 1923 || EditVariable.ClientCoID == 1684 || EditVariable.ClientCoID == 2205 || EditVariable.ClientCoID == 2257)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(dsConvenienceFee.Tables[0].Rows[0]["ID"])))
                    {
                        conv.ConvenienceYN = true;
                    }
                    else
                    {
                        conv.ConvenienceYN = false;
                    }
                    conv.AdditionalKm = Convert.ToInt32(dsConvenienceFee.Tables[0].Rows[0]["AddtionalKm"]);
                }
            }
            if (InstaCon.State != ConnectionState.Closed)
            {
                InstaCon.Close();
            }

            return conv;
        }

        public double GetBasic(EditClosingVariables EditVariable, Convenience conv)
        {
            double SubTotal = 0;

            if (EditVariable.Service == "C" && !EditVariable.OutstationYN && !EditVariable.CustomYN && !EditVariable.LocalOneWayDropYN)
            {
                if (EditVariable.ApplyHigherExtraAmt && EditVariable.CityID == 5)
                {
                    if (EditVariable.ExtraHrAmt >= EditVariable.ExtraKMAmt)
                    {
                        EditVariable.ExtraKMAmt = 0;
                    }
                    else
                    {
                        EditVariable.ExtraHrAmt = 0;
                    }


                }
                if (EditVariable.LocalOneWayDropYN)
                {
                    EditVariable.ConvenienceYN = false;
                }

                if (EditVariable.ConvenienceYN)
                {
                    if (EditVariable.DiscType != "Fixed")
                    {
                        EditVariable.DiscAmt = (((EditVariable.NoOfDys * EditVariable.PkgRate) + EditVariable.ExtraHrAmt
                            + EditVariable.ExtraKMAmt + EditVariable.FixedGarageRun
                            + (EditVariable.NoOfNgts * EditVariable.NightStayAllowance)) * EditVariable.DiscPC) / 100;
                    }

                    //if (EditVariable.DiscType == "Fixed")
                    //{
                    SubTotal = ((EditVariable.NoOfDys * EditVariable.PkgRate) + EditVariable.ExtraHrAmt
                        + EditVariable.ExtraKMAmt + EditVariable.FixedGarageRun + EditVariable.FuelAmount
                        + EditVariable.others + (EditVariable.NoOfNgts * EditVariable.NightStayAllowance)) - EditVariable.DiscAmt;
                    //}
                    //else
                    //{
                    //    SubTotal = (((EditVariable.NoOfDys * EditVariable.PkgRate) + EditVariable.ExtraHrAmt
                    //        + EditVariable.ExtraKMAmt + EditVariable.FixedGarageRun + EditVariable.FuelAmount
                    //        + EditVariable.others + (EditVariable.NoOfNgts * EditVariable.NightStayAllowance)) * (100 - EditVariable.DiscPC)) / 100;
                    //}
                }
                else
                {
                    double NightAmt;
                    if (EditVariable.LocalOneWayDropYN)
                    {
                        NightAmt = 0;
                    }
                    else
                    {
                        NightAmt = EditVariable.NoOfNgts * EditVariable.NightStayAllowance;
                    }

                    if (EditVariable.DiscType != "Fixed")
                    {
                        EditVariable.DiscAmt = ((EditVariable.PkgRate + EditVariable.ExtraHrAmt
                            + EditVariable.ExtraKMAmt + EditVariable.FixedGarageRun
                            + NightAmt) * EditVariable.DiscPC) / 100;
                    }

                    //if (EditVariable.DiscType == "Fixed")
                    //{
                    SubTotal = (EditVariable.PkgRate + EditVariable.ExtraHrAmt
                        + EditVariable.ExtraKMAmt + EditVariable.FixedGarageRun + EditVariable.FuelAmount
                        + EditVariable.others + NightAmt) - EditVariable.DiscAmt;
                    //}
                    //else
                    //{
                    //    SubTotal = ((EditVariable.PkgRate + EditVariable.ExtraHrAmt
                    //        + EditVariable.ExtraKMAmt + EditVariable.FixedGarageRun + EditVariable.FuelAmount
                    //        + EditVariable.others + NightAmt) * (100 - EditVariable.DiscPC)) / 100;
                    //}
                }
            }
            else if (EditVariable.Service == "C" && EditVariable.OutstationYN && !EditVariable.CustomYN)
            {
                if (EditVariable.ClientCoID != 1923 && EditVariable.ClientCoID != 1684)
                {
                    EditVariable.CorTax = 0;
                }

                if (EditVariable.DiscType != "Fixed")
                {
                    EditVariable.DiscAmt = (((EditVariable.PkgRate * EditVariable.PkgHrTrue) + EditVariable.ExtraHrAmt
                        + (EditVariable.OutStationAllowance * EditVariable.PkgHrTrue)
                        + EditVariable.ExtraKMAmt + EditVariable.FixedGarageRun
                        + (EditVariable.CorTax * EditVariable.PkgHrTrue)
                        + (EditVariable.NoOfNgts * EditVariable.NightStayAllowance)) * EditVariable.DiscPC) / 100;
                }

                //if (EditVariable.DiscType == "Fixed")
                //{
                SubTotal = ((EditVariable.PkgRate * EditVariable.PkgHrTrue) + EditVariable.ExtraHrAmt
                    + (EditVariable.OutStationAllowance * EditVariable.PkgHrTrue)
                    + EditVariable.ExtraKMAmt + EditVariable.FixedGarageRun + EditVariable.FuelAmount
                    + EditVariable.others + (EditVariable.CorTax * EditVariable.PkgHrTrue)
                    + (EditVariable.NoOfNgts * EditVariable.NightStayAllowance)) - EditVariable.DiscAmt;
                //}
                //else
                //{
                //    SubTotal = (((EditVariable.PkgRate * EditVariable.PkgHrTrue) + EditVariable.ExtraHrAmt
                //        + (EditVariable.OutStationAllowance * EditVariable.PkgHrTrue)
                //        + EditVariable.ExtraKMAmt + EditVariable.FixedGarageRun + EditVariable.FuelAmount
                //        + EditVariable.others + (EditVariable.CorTax * EditVariable.PkgHrTrue)
                //        + (EditVariable.NoOfNgts * EditVariable.NightStayAllowance)) * (100 - EditVariable.DiscPC)) / 100;
                //    //set gstsurcharge
                //}
            }
            else if (EditVariable.Service == "C" && !EditVariable.OutstationYN && EditVariable.CustomYN)
            {
                double TotalKMBilled = 0, Average = 0;
                TotalKMBilled = EditVariable.ThresholdExtraKM + (EditVariable.PkgHrTrue * 50);
                Average = TotalKMBilled / EditVariable.PkgHrTrue;
                if (Average > 175)
                {
                    if (EditVariable.DiscType != "Fixed")
                    {
                        EditVariable.DiscAmt = (((EditVariable.PkgRate * EditVariable.PkgHrTrue)
                            + ((EditVariable.ExtraKMRate + 1) * EditVariable.TotalKm) + EditVariable.FixedGarageRun
                            + (EditVariable.CorTax * EditVariable.PkgHrTrue)) * EditVariable.DiscPC) / 100;
                    }

                    //if (EditVariable.DiscType == "Fixed")
                    //{
                    SubTotal = ((EditVariable.PkgRate * EditVariable.PkgHrTrue)
                        + ((EditVariable.ExtraKMRate + 1) * EditVariable.TotalKm) + EditVariable.FixedGarageRun + EditVariable.FuelAmount
                        + EditVariable.others + (EditVariable.CorTax * EditVariable.PkgHrTrue)) - EditVariable.DiscAmt;
                    //}
                    //else
                    //{
                    //    SubTotal = (((EditVariable.PkgRate * EditVariable.PkgHrTrue)
                    //        + ((EditVariable.ExtraKMRate + 1) * EditVariable.TotalKm) + EditVariable.FixedGarageRun + EditVariable.FuelAmount
                    //        + EditVariable.others + (EditVariable.CorTax * EditVariable.PkgHrTrue)) * (100 - EditVariable.DiscPC)) / 100;
                    //    //set gstsurcharge
                    //}
                }
                else
                {
                    if (EditVariable.DiscType != "Fixed")
                    {
                        EditVariable.DiscAmt = (((EditVariable.PkgRate * EditVariable.PkgHrTrue)
                            + ((EditVariable.ExtraKMRate + 1) * EditVariable.TotalKm) + EditVariable.FixedGarageRun
                            + ((EditVariable.PkgHrTrue * 175) * EditVariable.PkgHrTrue)) * EditVariable.DiscPC) / 100;
                    }

                    //if (EditVariable.DiscType == "Fixed")
                    //{
                    SubTotal = ((EditVariable.PkgRate * EditVariable.PkgHrTrue)
                        + ((EditVariable.ExtraKMRate + 1) * EditVariable.TotalKm) + EditVariable.FixedGarageRun + EditVariable.FuelAmount
                        + EditVariable.others + ((EditVariable.PkgHrTrue * 175) * EditVariable.PkgHrTrue)) - EditVariable.DiscAmt;
                    //}
                    //else
                    //{
                    //    SubTotal = (((EditVariable.PkgRate * EditVariable.PkgHrTrue)
                    //        + ((EditVariable.ExtraKMRate + 1) * EditVariable.TotalKm) + EditVariable.FixedGarageRun + EditVariable.FuelAmount
                    //        + EditVariable.others + ((EditVariable.PkgHrTrue * 175) * EditVariable.PkgHrTrue)) * (100 - EditVariable.DiscPC)) / 100;
                    //    //set gstsurcharge
                    //}
                }
            }
            else if (EditVariable.Service == "A" && !EditVariable.LocalOneWayDropYN)
            {
                if (EditVariable.DiscType != "Fixed")
                {
                    EditVariable.DiscAmt = ((EditVariable.PkgRate + EditVariable.FixedGarageRun
                         + (EditVariable.NoOfNgts * EditVariable.NightStayAllowance)) * EditVariable.DiscPC) / 100;
                }

                //if (EditVariable.DiscType == "Fixed")
                //{
                SubTotal = (EditVariable.PkgRate + EditVariable.others + EditVariable.FixedGarageRun + EditVariable.FuelAmount
                     + (EditVariable.NoOfNgts * EditVariable.NightStayAllowance)) - EditVariable.DiscAmt;
                //}
                //else
                //{
                //    SubTotal = ((EditVariable.PkgRate + EditVariable.others + EditVariable.FixedGarageRun + EditVariable.FuelAmount
                //         + (EditVariable.NoOfNgts * EditVariable.NightStayAllowance)) * (100 - EditVariable.DiscPC)) / 100;
                //}
            }
            else if (EditVariable.Service == "T" && !EditVariable.LocalOneWayDropYN)
            {
                if (EditVariable.DiscType != "Fixed")
                {
                    EditVariable.DiscAmt = ((EditVariable.PkgRate + EditVariable.FixedGarageRun
                        + (EditVariable.NoOfNgts * EditVariable.NightStayAllowance)) * EditVariable.DiscPC) / 100;
                }

                //if (EditVariable.DiscType == "Fixed")
                //{
                SubTotal = (EditVariable.PkgRate + EditVariable.others + EditVariable.FixedGarageRun + EditVariable.FuelAmount
                     + (EditVariable.NoOfNgts * EditVariable.NightStayAllowance)) - EditVariable.DiscAmt;
                //}
                //else
                //{
                //    SubTotal = ((EditVariable.PkgRate + EditVariable.others + EditVariable.FixedGarageRun + EditVariable.FuelAmount
                //        + (EditVariable.NoOfNgts * EditVariable.NightStayAllowance)) * (100 - EditVariable.DiscPC)) / 100;
                //}
            }
            else if (EditVariable.LocalOneWayDropYN && EditVariable.IsMinuteWiseBilling)
            {
                if (EditVariable.DiscType != "Fixed")
                {
                    EditVariable.DiscAmt = ((EditVariable.PkgRate + EditVariable.FixedGarageRun
                    + EditVariable.ExtraHrAmt + EditVariable.ExtraKMAmt
                    + (EditVariable.NoOfNgts * EditVariable.NightStayAllowance)) * EditVariable.DiscPC) / 100;
                }

                SubTotal = (EditVariable.PkgRate + EditVariable.others + EditVariable.FixedGarageRun + EditVariable.FuelAmount
                    + EditVariable.ExtraHrAmt + EditVariable.ExtraKMAmt
                    + (EditVariable.NoOfNgts * EditVariable.NightStayAllowance)) - EditVariable.DiscAmt;
            }

            return SubTotal;
        }


        public bool VendorCarApprovedYN(int BookingID)
        {
            bool ApproveYN = true;
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();
            SqlConnection InstaCon = new SqlConnection(connectionString);
            SqlCommand checkVendorCarApprovedYN = InstaCon.CreateCommand();
            //checkVendorCarApprovedYN.CommandText = "SELECT top 1 ds.BookingID, ds.vendorcaryn, ds.vendorchauffyn, ds.carid, ds.chauffeurid, VCM.ApproveYN AS CarApproveYN, vchauf.ApproveYN as ChaufApproveYN, VCM.Active FROM CorintDS AS ds WITH (NOLOCK) INNER JOIN corintVendorcarmaster AS VCM WITH (NOLOCK) on ds.vendorcaryn = 1 and ds.carid = VCM.vendorcarid LEFT OUTER JOIN corintvendorchaufmaster as vchauf WITH (NOLOCK) ON DS.VendorChauffYN = 1 AND DS.ChauffeurID = VCHAUF.VendorChauffeurID WHERE ds.bookingid = @BookingID and ds.vendorcaryn = 1";
            checkVendorCarApprovedYN.CommandText = "SELECT top 1 VCM.ApproveYN AS CarApproveYN FROM CorintDS AS ds WITH (NOLOCK) INNER JOIN corintVendorcarmaster AS VCM WITH (NOLOCK) on ds.vendorcaryn = 1 and ds.carid = VCM.vendorcarid LEFT OUTER JOIN corintvendorchaufmaster as vchauf WITH (NOLOCK) ON DS.VendorChauffYN = 1 AND DS.ChauffeurID = VCHAUF.VendorChauffeurID WHERE ds.bookingid = @BookingID and ds.vendorcaryn = 1";
            checkVendorCarApprovedYN.Parameters.AddWithValue("@BookingID", BookingID);

            if (InstaCon.State != ConnectionState.Open)
            {
                InstaCon.Open();
            }
            DataSet dsVendorCarApprovedYN = new DataSet();
            SqlDataAdapter objAdapterVendorCarApprovedYN = new SqlDataAdapter(checkVendorCarApprovedYN);
            objAdapterVendorCarApprovedYN.Fill(dsVendorCarApprovedYN);

            if (dsVendorCarApprovedYN.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= dsVendorCarApprovedYN.Tables[0].Rows.Count - 1; i++)
                {
                    if (!Convert.ToBoolean(dsVendorCarApprovedYN.Tables[0].Rows[i]["CarApproveYN"]))
                    {
                        ApproveYN = false;
                    }
                }
            }

            if (InstaCon.State != ConnectionState.Closed)
            {
                InstaCon.Close();
            }

            return ApproveYN;
        }

        public bool CheckClosedDuty(int BookingID)
        {
            bool ManualClose = true;
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();
            SqlConnection InstaCon = new SqlConnection(connectionString);
            SqlCommand CheckClosedDuty = InstaCon.CreateCommand();
            CheckClosedDuty.CommandText = "SELECT top 1 confirmedittype FROM corintcordrivecloseddetails WHERE bookingid = @BookingID ";
            CheckClosedDuty.Parameters.AddWithValue("@BookingID", BookingID);

            if (InstaCon.State != ConnectionState.Open)
            {
                InstaCon.Open();
            }
            DataSet dsCheckClosedDuty = new DataSet();
            SqlDataAdapter objAdapterCheckClosedDuty = new SqlDataAdapter(CheckClosedDuty);
            objAdapterCheckClosedDuty.Fill(dsCheckClosedDuty);

            if (dsCheckClosedDuty.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= dsCheckClosedDuty.Tables[0].Rows.Count - 1; i++)
                {
                    if (Convert.ToString(dsCheckClosedDuty.Tables[0].Rows[i]["confirmedittype"]).Contains("Confirm"))
                    {
                        ManualClose = false;
                    }
                }
            }

            if (InstaCon.State != ConnectionState.Closed)
            {
                InstaCon.Close();
            }

            return ManualClose;
        }


        public OverLappingVariables CheckOverlapping(int BookingID, int ClientCoID, Int64 KmOut, Int64 KmIn, DateTime GuestOpDate, DateTime GuestClDate
            , string GTimeout, string GTimeClose, int CarID, bool VendorCarYN, bool OutStationYN)
        {
            OverLappingVariables ova = new OverLappingVariables();
            ova.OverLappingYN = false;
            ova.OverLappingMessage = "";

            if (!OutStationYN)
            {
                string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();

                SqlConnection InstaCon = new SqlConnection(connectionString);

                SqlCommand checkChargingLink = InstaCon.CreateCommand();
                checkChargingLink.CommandText = "select top 1 1 from CORIntInvoiceVoidHistory where NewBookingID =  @BookingID ";

                checkChargingLink.Parameters.AddWithValue("@BookingID", BookingID);
                if (InstaCon.State != ConnectionState.Open)
                {
                    InstaCon.Open();
                }
                DataSet ds = new DataSet();
                SqlDataAdapter objAdapter = new SqlDataAdapter(checkChargingLink);
                objAdapter.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ova.OverLappingYN = false;
                }

                //overlapping check start
                DataSet overlap = new DataSet();

                overlap = CheckOverlappingKM(CarID, VendorCarYN, KmOut, KmIn, ClientCoID, BookingID);

                if (overlap.Tables[0].Rows.Count > 0)
                {
                    ova.OverLappingYN = true;
                    ova.OverLappingMessage = "Please enter the correct KM as KM are overlapping with BookingID "
                        + overlap.Tables[0].Rows[0]["BookingID"].ToString()
                        + " where Km Out and Km IN were " + overlap.Tables[0].Rows[0]["KMOut"].ToString() + " and " + overlap.Tables[0].Rows[0]["KMIn"].ToString();
                }

                if (!ova.OverLappingYN)
                {
                    overlap = CheckOverlappingHr(CarID, VendorCarYN, GuestOpDate, GuestClDate, GTimeout, GTimeClose, ClientCoID, BookingID);

                    if (overlap.Tables[0].Rows.Count > 0)
                    {
                        ova.OverLappingYN = true;

                        ova.OverLappingMessage = "Please enter the correct Date Time as Date Time are overlapping with BookingID "
                            + overlap.Tables[0].Rows[0]["BookingID"].ToString()
                            + " where DateTime Out and DateTime in were " + overlap.Tables[0].Rows[0]["GuestOpDatetime"].ToString()
                            + " and " + overlap.Tables[0].Rows[0]["GuestClDatetime"].ToString();
                    }
                }

                //overlapping check end

                if (InstaCon.State != ConnectionState.Closed)
                {
                    InstaCon.Close();
                }
            }
            return ova;
        }

        public DataSet CheckOverlappingKM(int CarID, bool VendorCarYN, Int64 KMOut, Int64 KMIN, int ClientCoID, int BookingID)
        {
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@CarID", CarID);
            param[1] = new SqlParameter("@VendorCarYN", VendorCarYN);
            param[2] = new SqlParameter("@KMOut", KMOut);
            param[3] = new SqlParameter("@KMIN", KMIN);
            param[4] = new SqlParameter("@ClientCoID", ClientCoID);
            param[5] = new SqlParameter("@BookingID", BookingID);

            return SqlHelper.ExecuteDataset("CheckOverLappingKM", param);
        }

        public DataSet CheckOverlappingHr(int CarID, bool VendorCarYN, DateTime GuestOpDate, DateTime GuestClDate, string GTimeout
            , string GTimeIn, int ClientCoID, int BookingID)
        {
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@CarID", CarID);
            param[1] = new SqlParameter("@VendorCarYN", VendorCarYN);
            param[2] = new SqlParameter("@GuestOpDate", GuestOpDate);
            param[3] = new SqlParameter("@GuestClDate", GuestClDate);
            param[4] = new SqlParameter("@GTimeout", GTimeout);
            param[5] = new SqlParameter("@GTimeIn", GTimeIn);
            param[6] = new SqlParameter("@ClientCoID", ClientCoID);
            param[7] = new SqlParameter("@BookingID", BookingID);

            return SqlHelper.ExecuteDataset("CheckOverLappingTime", param);
        }

        public DataSet GetPreauthByBookingID(string OrderID)
        {
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();
            SqlConnection InstaCon = new SqlConnection(connectionString);
            SqlCommand checkVendorCarApprovedYN = InstaCon.CreateCommand();

            checkVendorCarApprovedYN.CommandText = "SELECT top 1 * from CorIntPreAuthStatus where OrderID = @OrderID ";
            checkVendorCarApprovedYN.Parameters.AddWithValue("@OrderID", OrderID);

            if (InstaCon.State != ConnectionState.Open)
            {
                InstaCon.Open();
            }
            DataSet dsVendorCarApprovedYN = new DataSet();
            SqlDataAdapter objAdapterVendorCarApprovedYN = new SqlDataAdapter(checkVendorCarApprovedYN);
            objAdapterVendorCarApprovedYN.Fill(dsVendorCarApprovedYN);

            return dsVendorCarApprovedYN;

        }

        public void BeforePreAuth(string OrderID, string PageID, string OldBookingID, string SendSMSLater
            , string ClientCoID, string SMSMobiles, string SMSMobiles1
            , string bitIsVIP, string hdnAcceleration_No, string SendMailLater, string LimoStatus, string IDMain, string FYear)
        {
            SqlParameter[] param = new SqlParameter[13];
            param[0] = new SqlParameter("@OrderID", OrderID);
            param[1] = new SqlParameter("@PageID", PageID);
            param[2] = new SqlParameter("@OldBookingID", OldBookingID);
            param[3] = new SqlParameter("@SendSMSLater", SendSMSLater);
            param[4] = new SqlParameter("@ClientCoID", ClientCoID);
            param[5] = new SqlParameter("@SMSMobiles", SMSMobiles);
            param[6] = new SqlParameter("@SMSMobiles1", SMSMobiles1);
            param[7] = new SqlParameter("@VIP", bitIsVIP);
            param[8] = new SqlParameter("@hdnAcceleration_No", hdnAcceleration_No);
            param[9] = new SqlParameter("@SendMailLater", SendMailLater);
            param[10] = new SqlParameter("@LimoStatus", LimoStatus);
            param[11] = new SqlParameter("@IDMain", IDMain);
            param[12] = new SqlParameter("@FYear", FYear);

            SqlHelper.ExecuteDataset("Prc_BookingDetailsBeforePreauth", param);
        }

        public void UpdatePreauthStatus(string ResponseCode, string ResponseMessage, string TransactionId, string Card, string CardType, string InvoiceNo, string Amount)
        {
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@OrderID", InvoiceNo);
            param[1] = new SqlParameter("@TransactionID", TransactionId);
            param[2] = new SqlParameter("@CCNo", Card);
            param[3] = new SqlParameter("@AuthCode", ResponseCode);
            param[4] = new SqlParameter("@PGId", TransactionId);
            param[5] = new SqlParameter("@ErrorCode", ResponseCode);
            param[6] = new SqlParameter("@RedirectParameter", ResponseMessage);

            SqlHelper.ExecuteDataset("Prc_UpdatePreAuthStatus", param);
        }

        public DataSet UpdateBookingPreauthStatus(string ResponseCode, string ResponseMessage, string TransactionId, string Card, string InvoiceNo, string Amount, string LimoStatus)
        {
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@OrderID", InvoiceNo);
            param[1] = new SqlParameter("@TransactionID", TransactionId);
            param[2] = new SqlParameter("@LimoStatus", LimoStatus);
            param[3] = new SqlParameter("@CCNo", Card);
            param[4] = new SqlParameter("@AuthCode", TransactionId);
            param[5] = new SqlParameter("@PGId", TransactionId);

            return SqlHelper.ExecuteDataset("SP_UpdatePreAuth", param);
        }

        public DataSet GetBookingSummary(string OrderID)
        {
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();
            SqlConnection InstaCon = new SqlConnection(connectionString);
            SqlCommand GetBookingSummaryCommand = InstaCon.CreateCommand();

            GetBookingSummaryCommand.CommandText = @"select CB.BookingID, CONVERT(varchar, CB.PickupDate, 103) as RentalDate, CM.CityName
        , Indiv.FName + ' ' + isnull(Indiv.LName,'') as GuestName, Indiv.ClientCoID, Indiv.ClientCoIndivID, CCM.PaymateSysRegCode 
        from CorIntCarBooking as CB 
        inner join CORIntCityMaster as CM on cb.PickUpCityID = CM.CityID 
        inner join CORIntClientCoIndivMaster as Indiv on CB.ClientCoIndivID = Indiv.ClientCoIndivID 
        inner join CorIntClientCoMaster as CCM on Indiv.ClientcoID = CCM.clientcoid
        where CB.TrackID = @OrderID ";
            GetBookingSummaryCommand.Parameters.AddWithValue("@OrderID", OrderID);

            if (InstaCon.State != ConnectionState.Open)
            {
                InstaCon.Open();
            }
            DataSet dsGetBookingSummary = new DataSet();
            SqlDataAdapter objAdapterGetBookingSummary = new SqlDataAdapter(GetBookingSummaryCommand);
            objAdapterGetBookingSummary.Fill(dsGetBookingSummary);

            return dsGetBookingSummary;
        }

        public bool IsBookingDispatched(string BookingID)
        {
            bool BookingDispatchedYN = false;

            string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();
            SqlConnection InstaCon = new SqlConnection(connectionString);
            SqlCommand checkBookingDispatchedYN = InstaCon.CreateCommand();

            checkBookingDispatchedYN.CommandText = "select DS.bookingid, isnull(Inv.TotalCost,0) as TotalCost from dbo.CORIntDS as DS with (nolock) inner join CORIntInvoice as Inv with (nolock) on Inv.BookingID = DS.BookingID where DS.BookingID = @BookingID ";
            checkBookingDispatchedYN.Parameters.AddWithValue("@BookingID", BookingID);

            if (InstaCon.State != ConnectionState.Open)
            {
                InstaCon.Open();
            }
            DataSet dsBookingDispatchedYN = new DataSet();
            SqlDataAdapter objAdapterBookingDispatchedYN = new SqlDataAdapter(checkBookingDispatchedYN);
            objAdapterBookingDispatchedYN.Fill(dsBookingDispatchedYN);

            if (dsBookingDispatchedYN.Tables[0].Rows.Count > 0)
            {
                BookingDispatchedYN = true;
            }
            return BookingDispatchedYN;
        }


        public string Updatesmsstatus(string Bookingid, string SMSStatus)
        {
            string returnStatus = "";
            try
            {
                string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();

                SqlConnection InstaCon = new SqlConnection(connectionString);

                SqlCommand UpdatesmsstatusCommand = InstaCon.CreateCommand();
                UpdatesmsstatusCommand.CommandText = "update CORIntCarBooking Set SMSStatus = @SMSStatus Where BookingID = @BookingID ";
                UpdatesmsstatusCommand.Parameters.AddWithValue("@BookingID", Bookingid);
                UpdatesmsstatusCommand.Parameters.AddWithValue("@SMSStatus", SMSStatus);

                if (InstaCon.State != ConnectionState.Open)
                {
                    InstaCon.Open();
                }
                if (UpdatesmsstatusCommand.ExecuteNonQuery() > 0)
                {
                    returnStatus = "Success";
                }
                else
                {
                    returnStatus = "Failure";
                }

                UpdatesmsstatusCommand.Dispose();

                if (InstaCon.State != ConnectionState.Closed)
                {
                    InstaCon.Close();
                }
            }
            catch (Exception ex)
            {
                returnStatus = ex.ToString();
            }
            finally
            {

            }
            return returnStatus;
        }

        public DataSet GetEInvoiceDetails()
        {
            //SqlParameter[] param = new SqlParameter[1];
            //param[0] = new SqlParameter("@", );
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetDetailEInvoice");
        }

        public DataSet GetEInvoiceHeader()
        {
            //SqlParameter[] param = new SqlParameter[1];
            //param[0] = new SqlParameter("@", );
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetDetailEInvoiceBulk");
        }

        public DataSet GetEInvoiceLines(int InvoiceID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@InvoiceID", InvoiceID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetDetailEInvoiceBulkLines", param);
        }

        public void SaveEInvoice(string InvoiceNo, string SignedQRCode, string AckDt, string AckNo, string Irn)
        {
            try
            {
                string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();
                SqlConnection InstaCon1 = new SqlConnection(connectionString);

                InstaCon1.Open();

                SqlCommand insertcommand = InstaCon1.CreateCommand();
                insertcommand.CommandText = @" insert into dbo.CORIntEInvoice(InvoiceNo, SignedQRCode, AckDt, AckNo, Irn, CreateDate) 
                values(@InvoiceNo, @SignedQRCode, @AckDt, @AckNo, @Irn, getdate())";
                insertcommand.Parameters.AddWithValue("@InvoiceNo", InvoiceNo);
                insertcommand.Parameters.AddWithValue("@SignedQRCode", SignedQRCode);
                insertcommand.Parameters.AddWithValue("@AckDt", AckDt);
                insertcommand.Parameters.AddWithValue("@AckNo", AckNo);
                insertcommand.Parameters.AddWithValue("@Irn", Irn);

                insertcommand.ExecuteNonQuery();
                if (InstaCon1 != null) InstaCon1.Close();
            }
            catch (Exception Ex)
            {
                ErrorLog.LogErrorToLogFile(Ex, "error in SaveEInvoice method:");
            }
        }
    }
}
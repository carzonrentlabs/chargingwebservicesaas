﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for PreauthValidation
/// </summary>
public class PreauthValidation
{
    public PreauthValidation()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public int BookingID { get; set; }
    public int ClientCoIndivID { get; set; }
    public string GuestName { get; set; }
    public string UnitName { get; set; }
    public DateTime dropoffdate { get; set; }
    public string CityName { get; set; }
    public double ApprovalAmt { get; set; }
    public string OldRegistrationStatus { get; set; }

    //public string ErrorCode { get; set; }
    //public string ErrorDesc { get; set; }
    //public string errorMessage { get; set; }
    //public string transactionNo { get; set; }
    //public string InvoiceNo { get; set; }
    //public double Amount { get; set; }
    //public string TravellerId { get; set; }

    //public string CaptureTransactionID { get; set; }
    //public double CaptureAmount { get; set; }

    //public string RefundTransactionID { get; set; }
    //public double RefundAmount { get; set; }

    //public string CCNo { get; set; }
    //public string AuthCode { get; set; }
    //public string PGId { get; set; }
    //public string CCTypeName { get; set; }
    //public int CCType { get; set; }
    public DataTable ObjDataTable { get; set; }
    public int? DbOperationStatus { get; set; }
    public string FaclitatorPhoneNo { get; set; }
    public string FacilitatorEmailId { get; set; }
    public int FacilitatorId { get; set; }
    //public string AdditionalEmaiId { get; set; }
    //public string AdditionalMobileNo { get; set; }
    public string MailStatus { get; set; }
    public string GuestPhoneNo { get; set; }
    public bool SmsStatus { get; set; }
    public DateTime PickUpDate { get; set; }
    public string PickUpTime { get; set; }

}

public class CommonConstant
{
    public const int SUCCEED = 1;
    public const int FAIL = -1;
    public const int INVALID = -11;
    public const int DUPLICATE = 0;
    public const int DEFAULT_ID = -99;
}
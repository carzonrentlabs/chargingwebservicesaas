﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TaxClass
/// </summary>
public class TaxClass
{
    public TaxClass()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public class TaxDetails
    {
        public string ServiceTaxPercent;
        public string EduCessPercent;
        public string HduCessPercent;
        public string DSTPercent;
        public string KMWisePackage;
        public string SwachhBharatTaxPercent;
        public string KrishiKalyanTaxPercent;
        public string GSTEnabledYN;
        public string CGSTPercent;
        public string SGSTPercent;
        public string IGSTPercent;
        public string ClientGSTId;
    }
}
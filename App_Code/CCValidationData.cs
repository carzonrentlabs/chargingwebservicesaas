﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CCValidationData
/// </summary>
public class CCValidationData
{
    public CCValidationData()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public int bookingMadeByIndiv { get; set; }
    public bool ChargedYN { get; set; }
    public bool CVVNumber { get; set; }
    public int CCType { get; set; }
    public int ClientCoID { get; set; }
    public bool AccentureClosing { get; set; }

}
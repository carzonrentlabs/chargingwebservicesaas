﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Newtonsoft.Json;
using System.Collections.ObjectModel;
/// <summary>
/// Summary description for JsonResponse
/// </summary>
public class JsonResponse
{
    public JsonResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string status { get; set; }
    public string error_message { get; set; }
    public string transaction_id { get; set; }
    public string authorization_id { get; set; }
    public string receipt_number { get; set; }
    public string preauth_amount { get; set; }
    public string order_id { get; set; }
    public string order_info { get; set; }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CCValidationData
/// </summary>
public class EditClosingVariables
{
    public EditClosingVariables()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public int BookingID { get; set; }

    public DateTime DateOut { get; set; }
    public DateTime DateIn { get; set; }
    public string TimeOut { get; set; }
    public string TimeIn { get; set; }

    public Int64 GarageKmOut { get; set; }
    public Int64 GarageKmIn { get; set; }

    public Int64 GuestKmOut { get; set; }
    public Int64 GuestKmIn { get; set; }
    public DateTime GuestDateOut { get; set; }
    public DateTime GuestDateIn { get; set; }
    public string GuestTimeOut { get; set; }
    public string GuestTimeIn { get; set; }

    public double Parking { get; set; }
    public double Interstate { get; set; }
    public double others { get; set; }
    public string Remarks { get; set; }
    public bool NotifyGuestYN { get; set; }
    public int UserID { get; set; }

    public int ClientCoID { get; set; }
    public string Service { get; set; }
    public string BillingBasis { get; set; }
    public bool OutstationYN { get; set; }
    public bool CustomYN { get; set; }

    public double PkgHrTrue { get; set; }
    public double TotalHr { get; set; }
    public int IntNoNights { get; set; }
    public int IntNoDays { get; set; }
    public string PayMentMode { get; set; }

    public int CarCatID { get; set; }
    public double CorTax { get; set; }
    public int CityID { get; set; }
    public int CarModelID { get; set; }
    public string ClientType { get; set; }
    public int IndicatedPkgID { get; set; }

    public bool LocalOneWayDropYN { get; set; }
    public int Unitflag { get; set; }
    public Int64 PkgKmTrue { get; set; }
    public int TotalKm { get; set; }
    public int DropoffCityID { get; set; }
    public int ToNFro { get; set; }
    public int Flag { get; set; }
    public string ClosingStatus { get; set; }

    public int PkgID { get; set; }
    public double PkgRate { get; set; }
    public int PkgHrs { get; set; }
    public double PkgKMs { get; set; }
    public double ExtraHrRate { get; set; }
    public double ExtraKMRate { get; set; }

    public double ExtraHrAmt { get; set; }
    public double ExtraKMAmt { get; set; }

    public double OutStationAllowance { get; set; }
    public double NightStayAllowance { get; set; }
    public double ThresholdExtraHr { get; set; }
    public double ThresholdExtraKM { get; set; }
    public double WaitingCharges { get; set; }
    public double NoOfNgts { get; set; }
    public double NoOfDys { get; set; }
    public double ExNgtAmt { get; set; }
    public double ExdayAmt { get; set; }
    public double FxdTaxes { get; set; }
    public bool IsMinuteWiseBilling { get; set; }
    public bool NightChargeMultiplier { get; set; }
    public double CGSTTaxPercent { get; set; }
    public double SGSTTaxPercent { get; set; }
    public double IGSTTaxPercent { get; set; }
    public double ServiceTaxPercent { get; set; }
    public double EduCessPercent { get; set; }
    public double HduCessPercent { get; set; }
    public double SwachhBharatTaxPercent { get; set; }
    public double KrishiKalyanTaxPercent { get; set; }
    public double DSTPercent { get; set; }
    public int ClientGSTId { get; set; }
    public double GSTSurchargeAmount { get; set; }
    public DateTime GSTSurchargeEffectiveDate { get; set; }
    public double TotalMin { get; set; }

    public DateTime StartDateTime { get; set; }
    public DateTime EndDateTime { get; set; }

    public Int64 TotalFGR { get; set; }
    public Int64 FGROut { get; set; }
    public Int64 FGRIn { get; set; }
    public double FixedGarageRun { get; set; }
    public double FGRFraction { get; set; }
    public double FuelAmount { get; set; }
    public bool VendorCarYN { get; set; }
    public int CarID { get; set; }
    public int SubsidiaryID { get; set; }
    public bool ApplyHigherExtraAmt { get; set; }
    public bool ConvenienceYN { get; set; }
    public double DiscPC { get; set; }
    public double DiscAmt { get; set; }
    public string DiscType { get; set; }
    public double Basic { get; set; }
    public double SubTotal { get; set; }

    public double ServiceTaxAmt { get; set; }
    public double EduTaxAmt { get; set; }
    public double HduTaxAmt { get; set; }
    public double SwachhBharatTaxAmt { get; set; }
    public double KrishiKalyanTaxAmt { get; set; }
    public double VatAmt { get; set; }
    public double CGSTTaxAmt { get; set; }
    public double SGSTTaxAmt { get; set; }
    public double IGSTTaxAmt { get; set; }
    public double TotalCost { get; set; }

    public string InterUnitYN { get; set; }

    public double ExtraAmount { get; set; }

    public string ApprovalNo { get; set; }
    public int CCType { get; set; }
    public string Trackid { get; set; }
    public bool PreAuthNotRequire { get; set; }
    public string CCNo { get; set; }
    public string ExpYYMM { get; set; }
    public string chargingstatus { get; set; }
    public string DSStatus { get; set; }
    public double VoucherAmt { get; set; }
    public string MerchantID { get; set; }
    public double CalculatedAmount { get; set; }

    public int CreatedBy { get; set; }
    public bool SendApprovalLinkYN { get; set; }

    public int ServiceTypeID { get; set; }

    public double FuelSurcharge { get; set; }
    public bool PendingYn { get; set; }
    public double SanatizationCharges { get; set; }
}
using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using ChargingWebService;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using ChecksumsTest;
//using ICSharpCode;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]


public class AuthHeader : System.Web.Services.Protocols.SoapHeader
{
    public string UserName;
    public string Password;
}

public class Service : System.Web.Services.WebService
{
    DataSet BK = new DataSet();
    DataSet DTClientBookDetails = new DataSet();
    DataSet SD = new DataSet();
    DataSet data = new DataSet();

    clsAdmin objAdmin = new clsAdmin();
    Adler32 adl = new Adler32();
    public AuthHeader Credentials;

    public Service()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    string Password = System.Configuration.ConfigurationManager.AppSettings["CorPassWord"];
    string userName = System.Configuration.ConfigurationManager.AppSettings["Username"];

    //Master visa code below
    [SoapHeader("Credentials")]
    [WebMethod(Description = "Used Internally")]
    public string GetCaptureStatusMaster(int BookingID, double Amount)
    {
        return objAdmin.GetCaptureStatusMaster(BookingID, Amount);
    }

    //Amex code below

    [SoapHeader("Credentials")]
    [WebMethod(Description = "Used Internally")]
    public string GetCaptureStatusAmex(int BookingID, double Amount)
    {
        return objAdmin.GetCaptureStatusAmex(BookingID, Amount);
    }

    [SoapHeader("Credentials")]
    [WebMethod(Description = "72 hour Amex Schedular (Pepsico, Microsoft etc)")]
    public void AutoCloseAmexDuties()
    {
        //if (userName == "Charge" && Password == "Charge")
        //{
        ErrorLog.LoginfoToLogFile("start", "AutoCloseAmexDuties");

        string ErrorLog1 = objAdmin.GetAmex72HoursAutoClosingDetails();

        ErrorLog.LoginfoToLogFile("End" + ";" + ErrorLog1, "AutoCloseAmexDuties");
        //}
    }

    [SoapHeader("Credentials")]
    [WebMethod(Description = "72 hour for All Credit Card Bookings")]
    public void AutoCloseCreditCardDuties()
    {
        //ErrorLog.LoginfoToLogFile("Start method AutoCloseAmexDuties()", "");
        //if (userName == "Charge" && Password == "Charge")
        //{
        ErrorLog.LoginfoToLogFile("start", "AutoCloseCreditCardDuties");

        string ErrorLog1 = objAdmin.GetCreditCard72HoursAutoClosingDetails();

        ErrorLog.LoginfoToLogFile("End" + ";" + ErrorLog1, "AutoCloseCreditCardDuties");
        //}
    }

    [SoapHeader("Credentials")]
    [WebMethod(Description = "Used Internally")]
    public string AmexCharging(int BookingId)
    {
        return objAdmin.GetAmexClosingDetails(BookingId);
    }

    [SoapHeader("Credentials")]
    [WebMethod(Description = "Used Internally")]
    public string MasterVisaCharging(int BookingId)
    {
        return objAdmin.GetMasterVisaChargingDetails(BookingId);
    }

    [SoapHeader("Credentials")]
    [WebMethod(Description = "Used Internally")]
    public string MasterVisaTrackOrderID(int BookingId)
    {
        return objAdmin.MasterVisaTrackOrderID(BookingId);
    }

    [SoapHeader("Credentials")]
    [WebMethod(Description = "Used Internally")]
    public string EditClosing(int BookingId, DateTime DateIn, string TimeIn,
        Int32 GarageKmOut, Int32 GarageKmIn, Int32 GuestKmOut, Int32 GuestKmIn
        , DateTime GuestDateOut, DateTime GuestDateIn, string GuestTimeOut, string GuestTimeIn, double Parking, double Interstate, double others
        , string Remarks, bool NotifyGuestYN, int UserID)
    {
        //if (userName == "Charge" && Password == "Charge")
        //{
        ChargingWebService.DB db = new ChargingWebService.DB();
        EditClosingVariables EditVariable = new EditClosingVariables();
        EditVariable.BookingID = BookingId;
        EditVariable.DateIn = DateIn;
        EditVariable.TimeIn = db.CorrectTimeLength(TimeIn);
        EditVariable.GarageKmOut = GarageKmOut;
        EditVariable.GarageKmIn = GarageKmIn;
        EditVariable.GuestKmOut = GuestKmOut;
        EditVariable.GuestKmIn = GuestKmIn;
        EditVariable.GuestDateOut = GuestDateOut;
        EditVariable.GuestDateIn = GuestDateIn;
        EditVariable.GuestTimeOut = db.CorrectTimeLength(GuestTimeOut);
        EditVariable.GuestTimeIn = db.CorrectTimeLength(GuestTimeIn);
        EditVariable.Parking = Parking;
        EditVariable.Interstate = Interstate;
        EditVariable.Remarks = Remarks;
        EditVariable.NotifyGuestYN = NotifyGuestYN;
        EditVariable.UserID = UserID;

        if (EditVariable.ClientCoID == 1527 && EditVariable.GuestDateOut < Convert.ToDateTime("2016-09-20"))
        {
            EditVariable.BillingBasis = "gg";
        }
        return objAdmin.ClosingManualBooking(EditVariable);
    }



    [SoapHeader("Credentials")]
    [WebMethod(Description = "Used Internally")]
    public string ConfirmClosing(int BookingId, bool NotifyGuestYN)
    {
        int UserID = 1;
        string ChargingStatus = "";
        //if (userName == "Charge" && Password == "Charge")
        //{
        //return objAdmin.CloseBooking(BookingId, "Confirm", NotifyGuestYN, 1);
        //}
        //return "Error in Method";
        try
        {
            ErrorLog.LoginfoToLogFile("start:BookingId:" + BookingId.ToString()
                + ",NotifyGuestYN:" + NotifyGuestYN.ToString() + ",UserID" + UserID.ToString(), "ConfirmClosing");

            ChargingStatus = objAdmin.CloseBooking(BookingId, "Confirm", NotifyGuestYN, UserID);

            ErrorLog.LoginfoToLogFile("end:BookingId:" + BookingId.ToString()
                + ",NotifyGuestYN:" + NotifyGuestYN.ToString() + ",UserID" + UserID.ToString() + ",ChargingStatus:" + ChargingStatus, "ConfirmClosing");
        }
        catch (Exception Ex)
        {
            ChargingStatus = Ex.ToString();

            ErrorLog.LoginfoToLogFile("BookingId:" + BookingId.ToString()
                + ",NotifyGuestYN:" + NotifyGuestYN.ToString() + ",UserID" + UserID.ToString() + ",ChargingStatus:" + ChargingStatus, "ConfirmClosing");
        }
        return ChargingStatus;
    }

    [SoapHeader("Credentials")]
    [WebMethod(Description = "Used Internally")]
    public string ConfirmClosing_User(int BookingId, bool NotifyGuestYN, int UserID)
    {
        string ChargingStatus = "";
        try
        {
            ErrorLog.LoginfoToLogFile("start:BookingId:" + BookingId.ToString()
                + ",NotifyGuestYN:" + NotifyGuestYN.ToString() + ",UserID" + UserID.ToString(), "ConfirmClosing_User");

            ChargingStatus = objAdmin.CloseBooking(BookingId, "Confirm", NotifyGuestYN, UserID);

            ErrorLog.LoginfoToLogFile("end:BookingId:" + BookingId.ToString()
                + ",NotifyGuestYN:" + NotifyGuestYN.ToString() + ",UserID" + UserID.ToString() + ",ChargingStatus:" + ChargingStatus, "ConfirmClosing_User");
        }
        catch (Exception Ex)
        {
            ChargingStatus = Ex.ToString();

            ErrorLog.LoginfoToLogFile("BookingId:" + BookingId.ToString()
                + ",NotifyGuestYN:" + NotifyGuestYN.ToString() + ",UserID" + UserID.ToString() + ",ChargingStatus:" + ChargingStatus, "ConfirmClosing_User");
        }
        return ChargingStatus;
        //}
        //return "Error in Method";
    }

    [SoapHeader("Credentials")]
    [WebMethod(Description = "Register Corporate to save card")]
    public string PaymateCorporateRegistration(int ClientCoId)
    {
        string returnstatus = "";
        try
        {
            returnstatus = objAdmin.PaymateCorporateRegistration(ClientCoId);
        }
        catch (Exception ex)
        {
            returnstatus = "Failure";
        }
        return returnstatus;
    }


    [WebMethod(Description = "Preauth Process for Google Saved Card for Master / Visa")]
    public string PaymateCorporateGooglePreauth(int BookingID, double Amt)
    {
        ErrorLog.LoginfoToLogFile("start:BookingId: " + BookingID.ToString() + ",PreauthAmt:" + Amt.ToString(), "PaymateCorporateGooglePreauth");
        string returnstatus = "";
        try
        {
            returnstatus = objAdmin.PaymateCorporateGooglePreauth(BookingID, Amt);
        }
        catch (Exception ex)
        {
            ErrorLog.LoginfoToLogFile("BookingId: " + BookingID.ToString() + ",PreauthAmt:" + Amt.ToString() + ", exception: " + ex, "PaymateCorporateGooglePreauth");
            returnstatus = ex.ToString();
        }
        ErrorLog.LoginfoToLogFile("end:BookingId: " + BookingID.ToString() + ",PreauthAmt:" + Amt.ToString(), "PaymateCorporateGooglePreauth");
        return returnstatus;
    }

    //[SoapHeader("Credentials")]
    [WebMethod(Description = "New Registration Charging Process for Master / Visa / Amex")]
    public string NewRegistrationChargingProcess(int BookingId, double TotalCost)
    {
        ErrorLog.LoginfoToLogFile("start:BookingId: " + BookingId.ToString() + ",TotalCost:" + TotalCost.ToString(), "NewRegistrationChargingProcess");
        string returnstatus = "";
        try
        {
            PreAuthResult PVC = new PreAuthResult();
            PVC = objAdmin.NewRegistrationChargingProcess(BookingId, TotalCost);
            if (string.IsNullOrEmpty(PVC.transactionNo))
            {
                returnstatus = PVC.ChargingStatus;
            }
            else
            {
                returnstatus = PVC.ChargingStatus + ";" + PVC.transactionNo;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.LoginfoToLogFile("BookingId: " + BookingId.ToString() + ",TotalCost:" + TotalCost.ToString() + ", exception: " + ex, "NewRegistrationChargingProcess");
            returnstatus = ex.ToString();
        }
        ErrorLog.LoginfoToLogFile("end:BookingId: " + BookingId.ToString() + ",TotalCost:" + TotalCost.ToString(), "NewRegistrationChargingProcess");
        return returnstatus;
    }

    [SoapHeader("Credentials")]
    [WebMethod(Description = "New Registration Preauth Process for Master / Visa / Amex Only")]
    public string NewRegistrationPreauthProcess(int BookingId)
    {
        PreAuthResult CV = new PreAuthResult();
        string status = "";
        CV = objAdmin.NewRegistrationPreauthProcess(BookingId);
        if (string.IsNullOrEmpty(CV.errorMessage))
        {
            status = CV.ErrorCode;
        }
        else
        {
            status = CV.ErrorCode + ";" + CV.errorMessage;
        }
        return status;
    }

    [SoapHeader("Credentials")]
    [WebMethod(Description = "New Refund Process")]
    public string RefundProcess(int BookingId, double RefundAmount, double TotalCost)
    {
        string status = "";
        status = objAdmin.RefundProcess(BookingId, RefundAmount, TotalCost);
        return status;
    }

    //[SoapHeader("Credentials")]
    [WebMethod(Description = "Charging Web Service (if return Success then continue else failure and show return message)")]
    public string ChargingWebService(int BookingID, int UserID, bool NotifyGuestYN)
    {
        string status = "";
        //status = objAdmin.ChargingWebService(BookingID, UserID, NotifyGuestYN, false);
        status = ChargingWebService_New(BookingID, UserID, NotifyGuestYN, true);
        return status;
    }

    [WebMethod(Description = "Charging Web Service (if return Success then continue else failure and show return message)")]
    public string ChargingWebService_New(int BookingID, int UserID, bool NotifyGuestYN, bool ChargeYN)
    {
        string status = "";
        status = objAdmin.ChargingWebService(BookingID, UserID, NotifyGuestYN, ChargeYN);
        return status;
    }

    [WebMethod(Description = "Mail Consolidated Part")]
    public string InvoiceMailer(int BookingID)
    {
        string MailStatus = "";
        try
        {
            MailStatus = objAdmin.SendInvoiceMail(BookingID, false);
        }
        catch (Exception ex)
        {
            MailStatus = ex.ToString();
        }
        return MailStatus;
    }

    [WebMethod(Description = "One Way Closing")]
    public string OneWayClosing(Int32 bookingId, Int32 carCatID, DateTime dateIn, string timeIn, Int32 modelId, string waitingTimeHours, string waitingTimeMinute, double parking = 0, double interStateTax = 0)
    {
        ClosingVariables objClosingOL = new ClosingVariables();
        clsAdmin objAdmin = new clsAdmin();
        objClosingOL.BookingId = bookingId;
        objClosingOL.CarCatId = carCatID;
        //objClosing.PickupCityID = pickupcityId;
        // objClosing.Service = clientServicType;
        // objClosing.ClientCoID = clientCoId;
        //objClosing.DateOut = dateOut;
        objClosingOL.DateIn = dateIn;
        objClosingOL.TimeIn = timeIn;
        objClosingOL.CarModelId = modelId;
        //objClosing.DropOffCityId = dropOffCityId;
        objClosingOL.WaitingHr = waitingTimeHours;
        objClosingOL.WaitingMi = waitingTimeMinute;
        objClosingOL.ParkTollChages = Convert.ToInt32(parking);
        objClosingOL.InterstateTax = Convert.ToInt32(interStateTax);
        objClosingOL = objAdmin.GetOneWayClosing(objClosingOL);

        return objClosingOL.BasicRevenue.ToString() + "||" + objClosingOL.WaitingCharge.ToString();
    }

    [WebMethod(Description = "CheckPaymateRegistrationStatus")]
    public string PaymateCorporateRegistrationStatusCheck(int ClientCoIndivID)
    {
        string RegistrationStatus = "";
        clsAdmin objAdmin = new clsAdmin();
        RegistrationStatus = objAdmin.PaymateCorporateRegistrationStatusCheck(ClientCoIndivID);
        return RegistrationStatus;
    }

    [WebMethod(Description = "CheckPaymateRegistrationStatus")]
    public string PaymateCorporateChargingStatusCheck(int BookingID, bool UpdateBookingYN)
    {
        string RegistrationStatus = "";
        clsAdmin objAdmin = new clsAdmin();
        RegistrationStatus = objAdmin.PaymateCorporateChargingStatusCheck(BookingID, UpdateBookingYN);
        return RegistrationStatus;
    }

    [WebMethod(Description = "Master Visa Charging For Accenture")]
    public string AccentureChargingMethod(int BookingID, double TotalCost, string Misc1)
    {
        string ChargingStatus = "";
        clsAdmin objAdmin = new clsAdmin();
        ChargingStatus = objAdmin.checkChargingStatus(BookingID, TotalCost, Misc1);
        return ChargingStatus;
    }

    [WebMethod(Description = "Master Visa Charging For Accenture")]
    public string AccentureCheckChargingMethod(int BookingID, double TotalCost, string Misc1)
    {
        string ChargingStatus = "";
        clsAdmin objAdmin = new clsAdmin();
        ChargingStatus = objAdmin.checkChargingStatusnew(BookingID, TotalCost, Misc1);
        return ChargingStatus;

    }

    [WebMethod(Description = "failed Charging scheduler")]
    public void BulkCharging()
    {
        try
        {
            clsAdmin objAdmin = new clsAdmin();
            objAdmin.BulkCharging();
        }
        catch (Exception ex)
        {
            ErrorLog.LogErrorToLogFile(ex, "BulkCharging");
        }
    }

    [WebMethod(Description = "Bulk Charging Link to Send")]
    public void BulkChargingLink()
    {
        try
        {
            clsAdmin objAdmin = new clsAdmin();
            objAdmin.BulkChargingLink();
        }
        catch (Exception ex)
        {
            ErrorLog.LogErrorToLogFile(ex, "BulkChargingLink");
        }
    }

    [WebMethod(Description = "checking Charing status")]
    public string CheckChargestatus(string orderid, string TransactionID, bool OldPreauthModule)
    {
        string Chargingstatus = "";
        clsAdmin objAdmin = new clsAdmin();
        Chargingstatus = objAdmin.checkCharge(orderid, TransactionID, OldPreauthModule);
        return Chargingstatus;
    }

    //[WebMethod(Description = "test")]
    public string testing()
    {
        string test = "";
        DateTime currentdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        DateTime gstapplydate = new DateTime(2017, 6, 24);
        if (currentdate >= gstapplydate)
        {
            test = "ok";
        }
        else
        {
            test = "not ok";
        }

        return test;
    }

    [WebMethod]
    public bool CheckChargingLink(int bookingid)
    {
        ChargingWebService.DB db = new ChargingWebService.DB();
        return db.CheckChargingLink(bookingid);
    }

    [WebMethod]
    public string CheckclosingAllowed(int ClientCoID, string PaymentMode)
    {
        ChargingWebService.DB db = new ChargingWebService.DB();
        string closingAllowed = "";
        try
        {
            closingAllowed = db.CheckClosingAllowed(ClientCoID, PaymentMode);
        }
        catch (Exception ex)
        {
            closingAllowed = "";
        }
        return closingAllowed;
    }

    [WebMethod]
    public void RechargingUnAuthorized()
    {
        ChargingWebService.DB db = new ChargingWebService.DB();
        db.RechargingUnAuthorized();
    }

    [WebMethod]
    public void BulkCorporateClosing()
    {
        ChargingWebService.DB db = new ChargingWebService.DB();
        db.BulkCorporateClosing();
    }

    [WebMethod]
    public string CloseWithOutParkingToll(int BookingID, int UserId, bool NotifyGuestYN)
    {
        ChargingWebService.ChargingDetails ch = new ChargingWebService.ChargingDetails();
        string status = "";
        status = ch.CloseWithOutParkingToll(BookingID, UserId, NotifyGuestYN);

        return status;
    }

    [WebMethod]
    public bool VendorCarApprovedYN(int BookingID)
    {
        bool ApproveYN = true;
        try
        {
            ChargingWebService.DB db = new ChargingWebService.DB();
            ApproveYN = db.VendorCarApprovedYN(BookingID);
        }
        catch (Exception ex)
        {
            ErrorLog.LogErrorToLogFile(ex, "VendorCarApprovedYN");
            ApproveYN = true;
        }
        return ApproveYN;
    }

    [WebMethod]
    public string checkChargingStatus_AccentureNew(int BookingID, double TotalCost, string Misc1)
    {
        clsAdmin admin = new clsAdmin();
        string accentureChargingStatus = "";

        accentureChargingStatus = admin.checkChargingStatus_AccentureNew(BookingID, TotalCost, Misc1);

        ErrorLog.LoginfoToLogFile("accentureChargingStatus:" + accentureChargingStatus, "checkChargingStatus_AccentureNew");

        return accentureChargingStatus;
    }

    //[WebMethod]
    //public string MVPreauth_ClassicAspIntegration(string ApprovalNo, string AgentEmailId, string InvoiceNo,
    //        double Amount, DateTime RentalDate, string RentalType, string PickupCity, string CustomerName)
    //{
    //    clsAdmin objAdmin = new clsAdmin();
    //    return objAdmin.MVPreauth_ClassicAspIntegration(ApprovalNo, AgentEmailId, InvoiceNo, Amount, RentalDate, RentalType, PickupCity, CustomerName);
    //}
}
using System;

namespace ChecksumsTest
{

    //internal sealed class Adler32 : IChecksum
    internal sealed class Adler32
    {
        readonly static uint BASE = 65521;
        uint checksum;
        public Adler32()
        {
            Reset();
        }
        public void Reset()
        {
            checksum = 1;
        }
        public string Update(byte[] buffer)
        {
            string text = Update(buffer, 0, buffer.Length);
            return text;
        }
        public string Update(byte[] buf, int off, int len)
        {
            if (buf == null)
            {
                throw new ArgumentNullException("buf");
            }
            if (off < 0 || len < 0 || off + len > buf.Length)
            {
                throw new ArgumentOutOfRangeException();
            }
            uint s1 = checksum & 0xFFFF;
            uint s2 = checksum >> 16;
            while (len > 0)
            {
                int n = 3800;
                if (n > len)
                {
                    n = len;
                }
                len -= n;
                while (--n >= 0)
                {
                    s1 = s1 + (uint)(buf[off++] & 0xFF);
                    s2 = s2 + s1;
                }
                s1 %= BASE;
                s2 %= BASE;
            }
            checksum = (s2 << 16) | s1;
            return Convert.ToString(checksum);
        }
    }
}



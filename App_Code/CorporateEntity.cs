﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for CorporateEntity
/// </summary>
public class CorporateEntity
{
    public CorporateEntity()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static CorporateEntity Create(DataTable dataTable)
    {
        //
        // TODO: Add constructor logic here
        //
        if (dataTable != null && dataTable.Rows != null && dataTable.Rows.Count > 0)
        {
            DataRow row = dataTable.Rows[0];
            CorporateEntity entity = new CorporateEntity();
            entity.ClientCoId = Convert.ToInt32(row["ClientCoId"]);
            entity.ClientCoName = Convert.ToString(row["ClientCoName"]);
            entity.ClientAdminName = Convert.ToString(row["Contact1FName"]);
            entity.ClientContactNumber = Convert.ToString(row["Contact1Ph"]);
            entity.ClientAdminEmailId = Convert.ToString(row["EmailId"]);
            entity.ClientCoAddress = Convert.ToString(row["ClientAddress"]);
            entity.SysRegCode = Convert.ToString(row["PaymateSysRegCode"]);
            return entity;
        }
        return null;
    }

    public int ClientCoId { get; set; }
    public string ClientCoName { get; set; }
    public string ClientCoAddress { get; set; }
    public string ClientContactNumber { get; set; }
    public string ClientAdminName { get; set; }
    public string ClientAdminEmailId { get; set; }
    public string SysRegCode { get; set; }

}
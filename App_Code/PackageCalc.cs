﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DB
/// </summary>
namespace ChargingWebService
{
    public class PackageCalc
    {
        public PackageCalc()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet DS_GetPackageS1_ModelWise_Easycabs1(EditClosingVariables EditVariable)
        {
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@ClientType", EditVariable.ClientType);
            param[1] = new SqlParameter("@ClientID", EditVariable.ClientCoID);
            param[2] = new SqlParameter("@CarCatID", EditVariable.CarCatID);
            param[3] = new SqlParameter("@PkgHrs", EditVariable.TotalHr);
            param[4] = new SqlParameter("@CityID", EditVariable.CityID);
            param[5] = new SqlParameter("@DateOut", EditVariable.DateOut);
            param[6] = new SqlParameter("@ModelID", EditVariable.CarModelID);
            param[7] = new SqlParameter("@IndicatedPkgID", EditVariable.IndicatedPkgID);
            param[8] = new SqlParameter("@LocalOneWayDropYN", EditVariable.LocalOneWayDropYN);

            return SqlHelper.ExecuteDataset("ProcDS_GetPackageS1_ModelWise_Easycabs1", param);
        }

        public DataSet DS_GetPackageHr_ModelWise_Easycabs1(EditClosingVariables EditVariable)
        {
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@ClientType", EditVariable.ClientType);
            param[1] = new SqlParameter("@ClientID", EditVariable.ClientCoID);
            param[2] = new SqlParameter("@CarCatID", EditVariable.CarCatID);
            param[3] = new SqlParameter("@PkgHrs", EditVariable.TotalHr);
            param[4] = new SqlParameter("@CityID", EditVariable.CityID);
            param[5] = new SqlParameter("@DateOut", EditVariable.DateOut);
            param[6] = new SqlParameter("@ModelID", EditVariable.CarModelID);
            param[7] = new SqlParameter("@IndicatedPkgID", EditVariable.IndicatedPkgID);
            param[8] = new SqlParameter("@LocalOneWayDropYN", EditVariable.LocalOneWayDropYN);

            return SqlHelper.ExecuteDataset("ProcDS_GetPackageHr_ModelWise_Easycabs1", param);
        }

        public DataSet DS_GetPackageKms_With_Hrs_ModelWise1(EditClosingVariables EditVariable)
        {
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@ClientType", EditVariable.ClientType);
            param[1] = new SqlParameter("@ClientID", EditVariable.ClientCoID);
            param[2] = new SqlParameter("@CarCatID", EditVariable.CarCatID);
            param[3] = new SqlParameter("@PkgKMs", EditVariable.TotalKm);
            param[4] = new SqlParameter("@CityID", EditVariable.CityID);
            param[5] = new SqlParameter("@PkgHrs", EditVariable.TotalHr);
            param[6] = new SqlParameter("@DateOut", EditVariable.DateOut);
            param[7] = new SqlParameter("@ModelID", EditVariable.CarModelID);
            param[8] = new SqlParameter("@IndicatedPkgID", EditVariable.IndicatedPkgID);
            param[9] = new SqlParameter("@LocalOneWayDropYN", EditVariable.LocalOneWayDropYN);

            return SqlHelper.ExecuteDataset("ProcDS_GetPackageKms_With_Hrs_ModelWise1", param);
        }

        public DataSet DS_GetPackageKMS1_ModelWise_Easycabs1(EditClosingVariables EditVariable)
        {
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@ClientType", EditVariable.ClientType);
            param[1] = new SqlParameter("@ClientID", EditVariable.ClientCoID);
            param[2] = new SqlParameter("@CarCatID", EditVariable.CarCatID);
            param[3] = new SqlParameter("@PkgKMs", EditVariable.TotalKm);
            param[4] = new SqlParameter("@CityID", EditVariable.CityID);
            param[5] = new SqlParameter("@DateOut", EditVariable.DateOut);
            param[6] = new SqlParameter("@ModelID", EditVariable.CarModelID);
            param[7] = new SqlParameter("@IndicatedPkgID", EditVariable.IndicatedPkgID);
            param[8] = new SqlParameter("@LocalOneWayDropYN", EditVariable.LocalOneWayDropYN);

            return SqlHelper.ExecuteDataset("ProcDS_GetPackageKMS1_ModelWise_Easycabs1", param);
        }

        public DataSet DS_GetPackageKM_ModelWise_Easycabs1(EditClosingVariables EditVariable)
        {
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@ClientType", EditVariable.ClientType);
            param[1] = new SqlParameter("@ClientID", EditVariable.ClientCoID);
            param[2] = new SqlParameter("@CarCatID", EditVariable.CarCatID);
            param[3] = new SqlParameter("@PkgKMs", EditVariable.TotalKm);
            param[4] = new SqlParameter("@CityID", EditVariable.CityID);
            param[5] = new SqlParameter("@DateOut", EditVariable.DateOut);
            param[6] = new SqlParameter("@ModelID", EditVariable.CarModelID);
            param[7] = new SqlParameter("@IndicatedPkgID", EditVariable.IndicatedPkgID);
            param[8] = new SqlParameter("@LocalOneWayDropYN", EditVariable.LocalOneWayDropYN);

            return SqlHelper.ExecuteDataset("ProcDS_GetPackageKM_ModelWise_Easycabs1", param);
        }

        public DataSet DS_PackageOutStation_Intercity(EditClosingVariables EditVariable)
        {
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@CarCatID", EditVariable.CarCatID);
            param[1] = new SqlParameter("@CityID", EditVariable.CityID);
            param[2] = new SqlParameter("@ClientType", EditVariable.ClientType);
            param[3] = new SqlParameter("@ClientID", EditVariable.ClientCoID);
            param[4] = new SqlParameter("@DropOffCityID", EditVariable.DropoffCityID);
            param[5] = new SqlParameter("@ToNFro", EditVariable.ToNFro);
            param[6] = new SqlParameter("@pkgID", EditVariable.IndicatedPkgID);
            param[7] = new SqlParameter("@Flag", EditVariable.Flag);
            param[8] = new SqlParameter("@LocalOneWayDropYN", EditVariable.LocalOneWayDropYN);

            return SqlHelper.ExecuteDataset("ProcDS_PackageOutStation_Intercity", param);
        }

        public DataSet DS_PackageOutStation_ModelWise1(EditClosingVariables EditVariable)
        {
            SqlParameter[] param = new SqlParameter[11];
            param[0] = new SqlParameter("@CarCatID", EditVariable.CarCatID);
            param[1] = new SqlParameter("@CityID", EditVariable.CityID);
            param[2] = new SqlParameter("@ClientType", EditVariable.ClientType);
            param[3] = new SqlParameter("@ClientID", EditVariable.ClientCoID);
            param[4] = new SqlParameter("@DropOffCityID", EditVariable.DropoffCityID);
            param[5] = new SqlParameter("@ToNFro", EditVariable.ToNFro);
            param[6] = new SqlParameter("@Flag", EditVariable.Flag);
            param[7] = new SqlParameter("@DateOut", EditVariable.DateOut);
            param[8] = new SqlParameter("@ModelID", EditVariable.CarModelID);
            param[9] = new SqlParameter("@IndicatedPkgID", EditVariable.IndicatedPkgID);
            param[10] = new SqlParameter("@LocalOneWayDropYN", EditVariable.LocalOneWayDropYN);

            return SqlHelper.ExecuteDataset("ProcDS_PackageOutStation_ModelWise1", param);
        }

        public DataSet CustomPkg_EasyCabs(EditClosingVariables EditVariable)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@PkgID", EditVariable.IndicatedPkgID);

            return SqlHelper.ExecuteDataset("Prc_CustomPkg_EasyCabs", param);
        }

        public DataSet DS_PackageAirport(EditClosingVariables EditVariable)
        {
            SqlParameter[] param = new SqlParameter[11];
            param[0] = new SqlParameter("@CarCatID", EditVariable.CarCatID);
            param[1] = new SqlParameter("@CityID", EditVariable.CityID);
            param[2] = new SqlParameter("@ClientType", EditVariable.ClientType);
            param[3] = new SqlParameter("@ClientID", EditVariable.ClientCoID);
            param[4] = new SqlParameter("@Flag", EditVariable.Flag);
            param[5] = new SqlParameter("@ToNFro", EditVariable.ToNFro);
            param[6] = new SqlParameter("@DateOut", EditVariable.DateOut);
            param[7] = new SqlParameter("@ModelID", EditVariable.CarModelID);
            param[8] = new SqlParameter("@IndicatedPkgID", EditVariable.IndicatedPkgID);
            param[9] = new SqlParameter("@PkgKMs", EditVariable.TotalKm);
            param[10] = new SqlParameter("@LocalOneWayDropYN", EditVariable.LocalOneWayDropYN);

            return SqlHelper.ExecuteDataset("ProcDS_PackageAirport", param);
        }

        public DataSet GetOneWayCityTransferPackage(EditClosingVariables EditVariable)
        {
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@CarCatID", EditVariable.CarCatID);
            param[1] = new SqlParameter("@CityID", EditVariable.CityID);
            param[2] = new SqlParameter("@ClientType", EditVariable.ClientType);
            param[3] = new SqlParameter("@ClientID", EditVariable.ClientCoID);
            param[4] = new SqlParameter("@DateOut", EditVariable.DateOut);
            param[5] = new SqlParameter("@ModelID", EditVariable.CarModelID);
            param[6] = new SqlParameter("@IndicatedPkgID", EditVariable.IndicatedPkgID);
            param[7] = new SqlParameter("@DropoffCityID", EditVariable.DropoffCityID);

            return SqlHelper.ExecuteDataset("GetOneWayCityTransferPackage", param);
        }

        public DataSet DS_PackageCty_ModelWise1(EditClosingVariables EditVariable)
        {
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@CarCatID", EditVariable.CarCatID);
            param[1] = new SqlParameter("@CityID", EditVariable.CityID);
            param[2] = new SqlParameter("@ClientType", EditVariable.ClientType);
            param[3] = new SqlParameter("@ClientID", EditVariable.ClientCoID);
            param[4] = new SqlParameter("@Flag", EditVariable.Flag);
            param[5] = new SqlParameter("@DateOut", EditVariable.DateOut);
            param[6] = new SqlParameter("@ModelID", EditVariable.CarModelID);
            param[7] = new SqlParameter("@IndicatedPkgID", EditVariable.IndicatedPkgID);

            return SqlHelper.ExecuteDataset("ProcDS_PackageCty_ModelWise1", param);
        }
    }
}
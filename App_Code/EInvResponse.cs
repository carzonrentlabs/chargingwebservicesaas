﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

public class EInvResponse
{
    public EInvResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /*
     {"transaction_id":"","STATUS":"SUCCESS","ewb_subSupplyType":"","FILE_TXN_ID":"202012161512340000957267699","transactionMode":"REG","plant":"","custom":"","STATUS_CODE":"1","InfoDtls":"","RESPONSE":{"Status":"ACT","EwbNo":null,"EwbDt":null,"Irn":"c7c51d44b3474e328927f1d72c9779d06d33cd7089f9d054900963c4448c4d1b","EwbValidTill":null,"Remarks":null,"SignedQRCode_Decrypted":{"data":{"SellerGstin":"29AAFPB7029M000","BuyerGstin":"29AAACH3235M1ZF","DocNo":"KA/2021/C/006234","DocTyp":"INV","DocDt":"16/12/2020","TotInvVal":1856,"ItemCnt":1,"MainHsnCode":"996412","Irn":"c7c51d44b3474e328927f1d72c9779d06d33cd7089f9d054900963c4448c4d1b","IrnDt":"2020-12-16 15:14:00"},"iss":"NIC"},"SignedQRCode":"eyJhbGciOiJSUzI1NiIsImtpZCI6IkVEQzU3REUxMzU4QjMwMEJBOUY3OTM0MEE2Njk2ODMxRjNDODUwNDciLCJ0eXAiOiJKV1QiLCJ4NXQiOiI3Y1Y5NFRXTE1BdXA5NU5BcG1sb01mUElVRWMifQ.eyJkYXRhIjoie1wiU2VsbGVyR3N0aW5cIjpcIjI5QUFGUEI3MDI5TTAwMFwiLFwiQnV5ZXJHc3RpblwiOlwiMjlBQUFDSDMyMzVNMVpGXCIsXCJEb2NOb1wiOlwiS0EvMjAyMS9DLzAwNjIzNFwiLFwiRG9jVHlwXCI6XCJJTlZcIixcIkRvY0R0XCI6XCIxNi8xMi8yMDIwXCIsXCJUb3RJbnZWYWxcIjoxODU2LFwiSXRlbUNudFwiOjEsXCJNYWluSHNuQ29kZVwiOlwiOTk2NDEyXCIsXCJJcm5cIjpcImM3YzUxZDQ0YjM0NzRlMzI4OTI3ZjFkNzJjOTc3OWQwNmQzM2NkNzA4OWY5ZDA1NDkwMDk2M2M0NDQ4YzRkMWJcIixcIklybkR0XCI6XCIyMDIwLTEyLTE2IDE1OjE0OjAwXCJ9IiwiaXNzIjoiTklDIn0.J_rLZcjXt0JNX20U0-ZbxUsLu4KQ8GbFyglWa1o6G4k2e7OWfKPLUy4jEwhAFzlr9MiibzrhLU1HwlD8kagNALHwpuCPTmwxox7YVNdixfsyhan3H2tEW0EpTu7GbR1yguRC-287SeYvqvzWNyGiByI5j-33m84UOJLO7XF5NV1s7LoZ-INTcdQsc9mG8UCxn8mnHXrnyxXNd8SuMFYddGQ1-_aix09EXcjTszK2EdQ4M4JlhsRDeNLNyD1rFbERFkqzQdr8jWJOT_rbcF3ETuq5WfMDBibTPGjgMkn-JV5Lk4rofKRixQFvUPI5-E6Ye_n1ODm4lusgeuAyeVEJgw","AckNo":112010034758163,"AckDt":"2020-12-16 15:14:00","SignedInvoice":{"data":{"AckNo":112010034758163,"AckDt":"2020-12-16 15:14:00","Irn":"c7c51d44b3474e328927f1d72c9779d06d33cd7089f9d054900963c4448c4d1b","Version":"1.1","TranDtls":{"TaxSch":"GST","SupTyp":"B2B","RegRev":"N"},"DocDtls":{"Typ":"INV","No":"KA/2021/C/006234","Dt":"16/12/2020"},"SellerDtls":{"Gstin":"29AAFPB7029M000","LglNm":"Carzonrent India Pvt. Ltd.","TrdNm":"Carzonrent India Pvt. Ltd.","Addr1":"Door No : 16 Ulsoor Cross RoadBehind BSNL Office Ulsoor, Bangaluru - 560 008","Addr2":"Door No : 16 Ulsoor Cross RoadBehind BSNL Office Ulsoor, Bangaluru - 560 008","Loc":"Bangalore","Pin":560008,"Stcd":"29","Em":"bangalore@carzonrent.com"},"BuyerDtls":{"Gstin":"29AAACH3235M1ZF","LglNm":"Accenture Solutions Pvt Ltd Bangalore - Direct","Pos":"29","Addr1":"IBC Knowledge Park, Tower A,B Bannerghatta Road, Bangalore 560029","Loc":"Bangalore","Pin":560008,"Stcd":"29"},"ItemList":[{"ItemNo":0,"SlNo":"1","IsServc":"Y","HsnCd":"996412","Qty":1,"FreeQty":0,"Unit":"OTH","UnitPrice":1768,"TotAmt":1768,"Discount":0,"PreTaxVal":1768,"AssAmt":1768,"GstRt":5,"IgstAmt":0,"CgstAmt":44.2,"SgstAmt":44.2,"CesRt":0,"CesAmt":0,"StateCesAmt":0,"StateCesNonAdvlAmt":0,"OthChrg":0,"TotItemVal":1856,"OrdLineRef":"0","OrgCntry":"IN"}],"ValDtls":{"AssVal":1768,"CgstVal":44.2,"SgstVal":44.2,"IgstVal":0,"CesVal":0,"StCesVal":0,"Discount":0,"OthChrg":0,"RndOffAmt":0,"TotInvVal":1856},"PayDtls":{"CrDay":0,"PaidAmt":0,"PaymtDue":0}},"iss":"NIC"}}}
     */
    public class Data
    {
        public string Status { get; set; }    //MaxLength[3]
        public string EwbNo { get; set; }    //MaxLength[16]
        public string EwbDt { get; set; }    //String [19] (Date format is ‘yyyy-MM-dd HH:mm:ss ’)
        public string IRN { get; set; }    //String [64]
        public string EwbValidTill { get; set; }    //String [19] (Date format is ‘yyyy-MM-dd HH:mm:ss ’)
        public string Remarks { get; set; }    //
        public string SignedQRCode { get; set; }    //String No max length (it depend upon request payload)
        public string AckNo { get; set; }    //maxlength [50]
        public string AckDt { get; set; }    //String [19] (Date format is ‘yyyy-MM-dd HH:mm:ss ’)
        public string SignedInvoice { get; set; }    //No max length (it depend upon request payload)
    }

    

    //public Data Data { get; set; }
    //public string InfoDtls { get; set; }    //String No max length (it depend upon Error Type)

    // public Data Data 
    //public string FILE_TXN_ID { get; set; }    //String [50]
    //public string transaction_id { get; set; }    //String maxlength=40
    //public string transactionMode { get; set; }    //String Length:3
    //public string plant { get; set; }    //String maxlength=40
    //public string custom { get; set; }    //String maxlength=40
    //public int ewb_subSupplyType { get; set; }    //integer (Length-2)
    //public bool STATUS_CODE { get; set; }    //maxlength [1]
    //

    //public string InfCd { get; set; }    //String maxlength=10
    //public string RESPONSE { get; set; }    //     
    //public string IRNDT { get; set; }    //String [19] (Date format is ‘yyyy-MM-dd HH:mm:ss ’)
    //public string SignedQRCode_Decrypted { get; set; }    //String No max length (it depend upon request payload)
    //public string iss { get; set; }    //MaxLength[3]
    // }
}

public class ErrorDetail
{
    public string ErrorCode { get; set; }
    public string ErrorMessage { get; set; }
}

public class RESPONSE
{
    public string AckDt { get; set; }
    public string AckNo { get; set; }
    public string EwbDt { get; set; }
    public string EwbNo { get; set; }
    public string EwbValidTill { get; set; }
    public string Irn { get; set; }
    public string Remarks { get; set; }
    public string SignedQRCode { get; set; }

}



public class EBilled
{
    public string ewb_subSupplyType { get; set; }
    public string FILE_TXN_ID { get; set; }
    public string InfoDtls { get; set; }
    public string plant { get; set; }
    public RESPONSE RESPONSE { get; set; }
    public List<ErrorDetail> ErrorDetails { get; set; }
    public string STATUS { get; set; }
    public string STATUS_CODE { get; set; }

}
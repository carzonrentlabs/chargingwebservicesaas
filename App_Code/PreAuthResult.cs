﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for PreAuthResult
/// </summary>
public class PreAuthResult
{
    public PreAuthResult()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string ErrorCode { get; set; }
    public string ErrorDesc { get; set; }
    public string errorMessage { get; set; }
    public string transactionNo { get; set; }
    public string InvoiceNo { get; set; }
    public double Amount { get; set; }
    public string TravellerId { get; set; }

    public string CaptureTransactionID { get; set; }
    public double CaptureAmount { get; set; }

    public string RefundTransactionID { get; set; }
    public double RefundAmount { get; set; }
    public string CCNo { get; set; }
    public string AuthCode { get; set; }
    public string PGId { get; set; }
    public string CCTypeName { get; set; }
    public int CCType { get; set; }
    public string ChargingStatus { get; set; }
    public string Response { get; set; }

}